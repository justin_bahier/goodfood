#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: Restaurant
#------------------------------------------------------------

CREATE TABLE Restaurant(
        IdRestaurant Int  Auto_increment  NOT NULL ,
        Nom          Varchar (30) NOT NULL ,
        Description  Varchar (250) NOT NULL ,
        Siret        Varchar (30) NOT NULL ,
        SiteInternet Varchar (30) NOT NULL ,
        Adresse      Varchar (30) NOT NULL
	,CONSTRAINT Restaurant_PK PRIMARY KEY (IdRestaurant)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Produit
#------------------------------------------------------------

CREATE TABLE Produit(
        IdProduit    Int  Auto_increment  NOT NULL ,
        Nom          Varchar (30) NOT NULL ,
        Description  Varchar (250) NOT NULL ,
        DateCreation Date NOT NULL ,
        Prix         Int NOT NULL ,
        Photo        Varchar (50) NOT NULL ,
        IdRestaurant Int NOT NULL
	,CONSTRAINT Produit_PK PRIMARY KEY (IdProduit)

	,CONSTRAINT Produit_Restaurant_FK FOREIGN KEY (IdRestaurant) REFERENCES Restaurant(IdRestaurant)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Utilisateur
#------------------------------------------------------------

CREATE TABLE Utilisateur(
        IdUtilisateur Int  Auto_increment  NOT NULL ,
        Email         Varchar (30) NOT NULL ,
        MotsPasse     Varchar (30) NOT NULL ,
        Pseudo        Varchar (30) NOT NULL ,
        Telephone     Varchar (30) NOT NULL ,
        Photo         Varchar (100) NOT NULL ,
        Nom           Varchar (30) NOT NULL ,
        Prenom        Varchar (30) NOT NULL ,
        Adresse       Varchar (30) NOT NULL ,
        CarteBancaire Varchar (50) NOT NULL
	,CONSTRAINT Utilisateur_PK PRIMARY KEY (IdUtilisateur)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Livreur
#------------------------------------------------------------

CREATE TABLE Livreur(
        IdLivreur Int  Auto_increment  NOT NULL ,
        Email     Varchar (30) NOT NULL ,
        MotsPasse Varchar (30) NOT NULL ,
        Pseudo    Varchar (30) NOT NULL ,
        Telephone Varchar (30) NOT NULL ,
        Photo     Varchar (100) NOT NULL ,
        Nom       Varchar (30) NOT NULL ,
        Prenom    Varchar (30) NOT NULL ,
        RIB       Varchar (30) NOT NULL
	,CONSTRAINT Livreur_PK PRIMARY KEY (IdLivreur)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Commande
#------------------------------------------------------------

CREATE TABLE Commande(
        IdCommande    Int  Auto_increment  NOT NULL ,
        Adresse       Varchar (30) NOT NULL ,
        Prix          Int NOT NULL ,
        Etats         Varchar (50) NOT NULL ,
        IdUtilisateur Int NOT NULL ,
        IdLivreur     Int NOT NULL
	,CONSTRAINT Commande_PK PRIMARY KEY (IdCommande)

	,CONSTRAINT Commande_Utilisateur_FK FOREIGN KEY (IdUtilisateur) REFERENCES Utilisateur(IdUtilisateur)
	,CONSTRAINT Commande_Livreur0_FK FOREIGN KEY (IdLivreur) REFERENCES Livreur(IdLivreur)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Menu
#------------------------------------------------------------

CREATE TABLE Menu(
        IdMenu      Int  Auto_increment  NOT NULL ,
        Nom         Varchar (50) NOT NULL ,
        Description Varchar (50) NOT NULL ,
        Prix        Int NOT NULL ,
        Photo       Varchar (50) NOT NULL
	,CONSTRAINT Menu_PK PRIMARY KEY (IdMenu)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: ProduitCommande
#------------------------------------------------------------

CREATE TABLE ProduitCommande(
        IdProduit  Int NOT NULL ,
        IdCommande Int NOT NULL
	,CONSTRAINT ProduitCommande_PK PRIMARY KEY (IdProduit,IdCommande)

	,CONSTRAINT ProduitCommande_Produit_FK FOREIGN KEY (IdProduit) REFERENCES Produit(IdProduit)
	,CONSTRAINT ProduitCommande_Commande0_FK FOREIGN KEY (IdCommande) REFERENCES Commande(IdCommande)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: MenuCommande
#------------------------------------------------------------

CREATE TABLE MenuCommande(
        IdCommande Int NOT NULL ,
        IdMenu     Int NOT NULL
	,CONSTRAINT MenuCommande_PK PRIMARY KEY (IdCommande,IdMenu)

	,CONSTRAINT MenuCommande_Commande_FK FOREIGN KEY (IdCommande) REFERENCES Commande(IdCommande)
	,CONSTRAINT MenuCommande_Menu0_FK FOREIGN KEY (IdMenu) REFERENCES Menu(IdMenu)
)ENGINE=InnoDB;

#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: Restaurant
#------------------------------------------------------------

CREATE TABLE Restaurant(
        IdRestaurant Int  Auto_increment  NOT NULL ,
        Nom          Varchar (30) NOT NULL ,
        Description  Varchar (250) NOT NULL ,
        Siret        Varchar (30) NOT NULL ,
        SiteInternet Varchar (30) NOT NULL ,
        Adresse      Varchar (30) NOT NULL
	,CONSTRAINT Restaurant_PK PRIMARY KEY (IdRestaurant)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Produit
#------------------------------------------------------------

CREATE TABLE Produit(
        IdProduit    Int  Auto_increment  NOT NULL ,
        Nom          Varchar (30) NOT NULL ,
        Description  Varchar (250) NOT NULL ,
        DateCreation Date NOT NULL ,
        Prix         Int NOT NULL ,
        Photo        Varchar (50) NOT NULL ,
        IdRestaurant Int NOT NULL
	,CONSTRAINT Produit_PK PRIMARY KEY (IdProduit)

	,CONSTRAINT Produit_Restaurant_FK FOREIGN KEY (IdRestaurant) REFERENCES Restaurant(IdRestaurant)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Utilisateur
#------------------------------------------------------------

CREATE TABLE Utilisateur(
        IdUtilisateur Int  Auto_increment  NOT NULL ,
        Email         Varchar (30) NOT NULL ,
        MotsPasse     Varchar (30) NOT NULL ,
        Pseudo        Varchar (30) NOT NULL ,
        Telephone     Varchar (30) NOT NULL ,
        Photo         Varchar (100) NOT NULL ,
        Nom           Varchar (30) NOT NULL ,
        Prenom        Varchar (30) NOT NULL ,
        Adresse       Varchar (30) NOT NULL ,
        CarteBancaire Varchar (50) NOT NULL
	,CONSTRAINT Utilisateur_PK PRIMARY KEY (IdUtilisateur)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Livreur
#------------------------------------------------------------

CREATE TABLE Livreur(
        IdLivreur Int  Auto_increment  NOT NULL ,
        Email     Varchar (30) NOT NULL ,
        MotsPasse Varchar (30) NOT NULL ,
        Pseudo    Varchar (30) NOT NULL ,
        Telephone Varchar (30) NOT NULL ,
        Photo     Varchar (100) NOT NULL ,
        Nom       Varchar (30) NOT NULL ,
        Prenom    Varchar (30) NOT NULL ,
        RIB       Varchar (30) NOT NULL
	,CONSTRAINT Livreur_PK PRIMARY KEY (IdLivreur)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Commande
#------------------------------------------------------------

CREATE TABLE Commande(
        IdCommande    Int  Auto_increment  NOT NULL ,
        Adresse       Varchar (30) NOT NULL ,
        Prix          Int NOT NULL ,
        Etats         Varchar (50) NOT NULL ,
        IdUtilisateur Int NOT NULL ,
        IdLivreur     Int NOT NULL
	,CONSTRAINT Commande_PK PRIMARY KEY (IdCommande)

	,CONSTRAINT Commande_Utilisateur_FK FOREIGN KEY (IdUtilisateur) REFERENCES Utilisateur(IdUtilisateur)
	,CONSTRAINT Commande_Livreur0_FK FOREIGN KEY (IdLivreur) REFERENCES Livreur(IdLivreur)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Menu
#------------------------------------------------------------

CREATE TABLE Menu(
        IdMenu      Int  Auto_increment  NOT NULL ,
        Nom         Varchar (50) NOT NULL ,
        Description Varchar (50) NOT NULL ,
        Prix        Int NOT NULL ,
        Photo       Varchar (50) NOT NULL
	,CONSTRAINT Menu_PK PRIMARY KEY (IdMenu)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: ProduitCommande
#------------------------------------------------------------

CREATE TABLE ProduitCommande(
        IdProduit  Int NOT NULL ,
        IdCommande Int NOT NULL
	,CONSTRAINT ProduitCommande_PK PRIMARY KEY (IdProduit,IdCommande)

	,CONSTRAINT ProduitCommande_Produit_FK FOREIGN KEY (IdProduit) REFERENCES Produit(IdProduit)
	,CONSTRAINT ProduitCommande_Commande0_FK FOREIGN KEY (IdCommande) REFERENCES Commande(IdCommande)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: MenuCommande
#------------------------------------------------------------

CREATE TABLE MenuCommande(
        IdCommande Int NOT NULL ,
        IdMenu     Int NOT NULL
	,CONSTRAINT MenuCommande_PK PRIMARY KEY (IdCommande,IdMenu)

	,CONSTRAINT MenuCommande_Commande_FK FOREIGN KEY (IdCommande) REFERENCES Commande(IdCommande)
	,CONSTRAINT MenuCommande_Menu0_FK FOREIGN KEY (IdMenu) REFERENCES Menu(IdMenu)
)ENGINE=InnoDB;

#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------


#------------------------------------------------------------
# Table: Restaurant
#------------------------------------------------------------

CREATE TABLE Restaurant(
        IdRestaurant Int  Auto_increment  NOT NULL ,
        Nom          Varchar (30) NOT NULL ,
        Description  Varchar (250) NOT NULL ,
        Siret        Varchar (30) NOT NULL ,
        SiteInternet Varchar (30) NOT NULL ,
        Adresse      Varchar (30) NOT NULL
	,CONSTRAINT Restaurant_PK PRIMARY KEY (IdRestaurant)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Produit
#------------------------------------------------------------

CREATE TABLE Produit(
        IdProduit    Int  Auto_increment  NOT NULL ,
        Nom          Varchar (30) NOT NULL ,
        Description  Varchar (250) NOT NULL ,
        DateCreation Date NOT NULL ,
        Prix         Int NOT NULL ,
        Photo        Varchar (50) NOT NULL ,
        IdRestaurant Int NOT NULL
	,CONSTRAINT Produit_PK PRIMARY KEY (IdProduit)

	,CONSTRAINT Produit_Restaurant_FK FOREIGN KEY (IdRestaurant) REFERENCES Restaurant(IdRestaurant)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Utilisateur
#------------------------------------------------------------

CREATE TABLE Utilisateur(
        IdUtilisateur Int  Auto_increment  NOT NULL ,
        Email         Varchar (30) NOT NULL ,
        MotsPasse     Varchar (30) NOT NULL ,
        Pseudo        Varchar (30) NOT NULL ,
        Telephone     Varchar (30) NOT NULL ,
        Photo         Varchar (100) NOT NULL ,
        Nom           Varchar (30) NOT NULL ,
        Prenom        Varchar (30) NOT NULL ,
        Adresse       Varchar (30) NOT NULL ,
        CarteBancaire Varchar (50) NOT NULL
	,CONSTRAINT Utilisateur_PK PRIMARY KEY (IdUtilisateur)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Livreur
#------------------------------------------------------------

CREATE TABLE Livreur(
        IdLivreur Int  Auto_increment  NOT NULL ,
        Email     Varchar (30) NOT NULL ,
        MotsPasse Varchar (30) NOT NULL ,
        Pseudo    Varchar (30) NOT NULL ,
        Telephone Varchar (30) NOT NULL ,
        Photo     Varchar (100) NOT NULL ,
        Nom       Varchar (30) NOT NULL ,
        Prenom    Varchar (30) NOT NULL ,
        RIB       Varchar (30) NOT NULL
	,CONSTRAINT Livreur_PK PRIMARY KEY (IdLivreur)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Commande
#------------------------------------------------------------

CREATE TABLE Commande(
        IdCommande    Int  Auto_increment  NOT NULL ,
        Adresse       Varchar (30) NOT NULL ,
        Prix          Int NOT NULL ,
        Etats         Varchar (50) NOT NULL ,
        IdUtilisateur Int NOT NULL ,
        IdLivreur     Int NOT NULL
	,CONSTRAINT Commande_PK PRIMARY KEY (IdCommande)

	,CONSTRAINT Commande_Utilisateur_FK FOREIGN KEY (IdUtilisateur) REFERENCES Utilisateur(IdUtilisateur)
	,CONSTRAINT Commande_Livreur0_FK FOREIGN KEY (IdLivreur) REFERENCES Livreur(IdLivreur)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Menu
#------------------------------------------------------------

CREATE TABLE Menu(
        IdMenu      Int  Auto_increment  NOT NULL ,
        Nom         Varchar (50) NOT NULL ,
        Description Varchar (50) NOT NULL ,
        Prix        Int NOT NULL ,
        Photo       Varchar (50) NOT NULL
	,CONSTRAINT Menu_PK PRIMARY KEY (IdMenu)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: ProduitCommande
#------------------------------------------------------------

CREATE TABLE ProduitCommande(
        IdProduit  Int NOT NULL ,
        IdCommande Int NOT NULL
	,CONSTRAINT ProduitCommande_PK PRIMARY KEY (IdProduit,IdCommande)

	,CONSTRAINT ProduitCommande_Produit_FK FOREIGN KEY (IdProduit) REFERENCES Produit(IdProduit)
	,CONSTRAINT ProduitCommande_Commande0_FK FOREIGN KEY (IdCommande) REFERENCES Commande(IdCommande)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: MenuCommande
#------------------------------------------------------------

CREATE TABLE MenuCommande(
        IdCommande Int NOT NULL ,
        IdMenu     Int NOT NULL
	,CONSTRAINT MenuCommande_PK PRIMARY KEY (IdCommande,IdMenu)

	,CONSTRAINT MenuCommande_Commande_FK FOREIGN KEY (IdCommande) REFERENCES Commande(IdCommande)
	,CONSTRAINT MenuCommande_Menu0_FK FOREIGN KEY (IdMenu) REFERENCES Menu(IdMenu)
)ENGINE=InnoDB;

