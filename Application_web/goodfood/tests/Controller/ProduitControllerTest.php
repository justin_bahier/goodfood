<?php

namespace App\Test\Controller;

use App\Entity\Produit;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProduitControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private EntityManagerInterface $manager;
    private EntityRepository $repository;
    private string $path = '/produit/';

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->manager = (static::getContainer()->get('doctrine'))->getManager();
        $this->repository = $this->manager->getRepository(Produit::class);

        foreach ($this->repository->findAll() as $object) {
            $this->manager->remove($object);
        }

        $this->manager->flush();
    }

    public function testIndex(): void
    {
        $crawler = $this->client->request('GET', $this->path);

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Produit index');

        // Use the $crawler to perform additional assertions e.g.
        // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
    }

    public function testNew(): void
    {
        $this->markTestIncomplete();
        $this->client->request('GET', sprintf('%snew', $this->path));

        self::assertResponseStatusCodeSame(200);

        $this->client->submitForm('Save', [
            'produit[nom]' => 'Testing',
            'produit[description]' => 'Testing',
            'produit[datecreation]' => 'Testing',
            'produit[prix]' => 'Testing',
            'produit[photo]' => 'Testing',
            'produit[quantite]' => 'Testing',
            'produit[idrestaurant]' => 'Testing',
            'produit[idmenu]' => 'Testing',
            'produit[idcommande]' => 'Testing',
        ]);

        self::assertResponseRedirects('/sweet/food/');

        self::assertSame(1, $this->getRepository()->count([]));
    }

    public function testShow(): void
    {
        $this->markTestIncomplete();
        $fixture = new Produit();
        $fixture->setNom('My Title');
        $fixture->setDescription('My Title');
        $fixture->setDatecreation('My Title');
        $fixture->setPrix('My Title');
        $fixture->setPhoto('My Title');
        $fixture->setQuantite('My Title');
        $fixture->setIdrestaurant('My Title');
        $fixture->setIdmenu('My Title');
        $fixture->setIdcommande('My Title');

        $this->repository->add($fixture, true);

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Produit');

        // Use assertions to check that the properties are properly displayed.
    }

    public function testEdit(): void
    {
        $this->markTestIncomplete();
        $fixture = new Produit();
        $fixture->setNom('Value');
        $fixture->setDescription('Value');
        $fixture->setDatecreation('Value');
        $fixture->setPrix('Value');
        $fixture->setPhoto('Value');
        $fixture->setQuantite('Value');
        $fixture->setIdrestaurant('Value');
        $fixture->setIdmenu('Value');
        $fixture->setIdcommande('Value');

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s/edit', $this->path, $fixture->getId()));

        $this->client->submitForm('Update', [
            'produit[nom]' => 'Something New',
            'produit[description]' => 'Something New',
            'produit[datecreation]' => 'Something New',
            'produit[prix]' => 'Something New',
            'produit[photo]' => 'Something New',
            'produit[quantite]' => 'Something New',
            'produit[idrestaurant]' => 'Something New',
            'produit[idmenu]' => 'Something New',
            'produit[idcommande]' => 'Something New',
        ]);

        self::assertResponseRedirects('/produit/');

        $fixture = $this->repository->findAll();

        self::assertSame('Something New', $fixture[0]->getNom());
        self::assertSame('Something New', $fixture[0]->getDescription());
        self::assertSame('Something New', $fixture[0]->getDatecreation());
        self::assertSame('Something New', $fixture[0]->getPrix());
        self::assertSame('Something New', $fixture[0]->getPhoto());
        self::assertSame('Something New', $fixture[0]->getQuantite());
        self::assertSame('Something New', $fixture[0]->getIdrestaurant());
        self::assertSame('Something New', $fixture[0]->getIdmenu());
        self::assertSame('Something New', $fixture[0]->getIdcommande());
    }

    public function testRemove(): void
    {
        $this->markTestIncomplete();
        $fixture = new Produit();
        $fixture->setNom('Value');
        $fixture->setDescription('Value');
        $fixture->setDatecreation('Value');
        $fixture->setPrix('Value');
        $fixture->setPhoto('Value');
        $fixture->setQuantite('Value');
        $fixture->setIdrestaurant('Value');
        $fixture->setIdmenu('Value');
        $fixture->setIdcommande('Value');

        $$this->manager->remove($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));
        $this->client->submitForm('Delete');

        self::assertResponseRedirects('/produit/');
        self::assertSame(0, $this->repository->count([]));
    }
}
