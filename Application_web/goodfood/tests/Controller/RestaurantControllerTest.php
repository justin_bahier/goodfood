<?php

namespace App\Test\Controller;

use App\Entity\Restaurant;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RestaurantControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private EntityManagerInterface $manager;
    private EntityRepository $repository;
    private string $path = '/restaurant/';

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->manager = (static::getContainer()->get('doctrine'))->getManager();
        $this->repository = $this->manager->getRepository(Restaurant::class);

        foreach ($this->repository->findAll() as $object) {
            $this->manager->remove($object);
        }

        $this->manager->flush();
    }

    public function testIndex(): void
    {
        $crawler = $this->client->request('GET', $this->path);

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Restaurant index');

        // Use the $crawler to perform additional assertions e.g.
        // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
    }

    public function testNew(): void
    {
        $this->markTestIncomplete();
        $this->client->request('GET', sprintf('%snew', $this->path));

        self::assertResponseStatusCodeSame(200);

        $this->client->submitForm('Save', [
            'restaurant[nom]' => 'Testing',
            'restaurant[description]' => 'Testing',
            'restaurant[siret]' => 'Testing',
            'restaurant[siteinternet]' => 'Testing',
            'restaurant[adresse]' => 'Testing',
        ]);

        self::assertResponseRedirects('/sweet/food/');

        self::assertSame(1, $this->getRepository()->count([]));
    }

    public function testShow(): void
    {
        $this->markTestIncomplete();
        $fixture = new Restaurant();
        $fixture->setNom('My Title');
        $fixture->setDescription('My Title');
        $fixture->setSiret('My Title');
        $fixture->setSiteinternet('My Title');
        $fixture->setAdresse('My Title');

        $this->repository->add($fixture, true);

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Restaurant');

        // Use assertions to check that the properties are properly displayed.
    }

    public function testEdit(): void
    {
        $this->markTestIncomplete();
        $fixture = new Restaurant();
        $fixture->setNom('Value');
        $fixture->setDescription('Value');
        $fixture->setSiret('Value');
        $fixture->setSiteinternet('Value');
        $fixture->setAdresse('Value');

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s/edit', $this->path, $fixture->getId()));

        $this->client->submitForm('Update', [
            'restaurant[nom]' => 'Something New',
            'restaurant[description]' => 'Something New',
            'restaurant[siret]' => 'Something New',
            'restaurant[siteinternet]' => 'Something New',
            'restaurant[adresse]' => 'Something New',
        ]);

        self::assertResponseRedirects('/restaurant/');

        $fixture = $this->repository->findAll();

        self::assertSame('Something New', $fixture[0]->getNom());
        self::assertSame('Something New', $fixture[0]->getDescription());
        self::assertSame('Something New', $fixture[0]->getSiret());
        self::assertSame('Something New', $fixture[0]->getSiteinternet());
        self::assertSame('Something New', $fixture[0]->getAdresse());
    }

    public function testRemove(): void
    {
        $this->markTestIncomplete();
        $fixture = new Restaurant();
        $fixture->setNom('Value');
        $fixture->setDescription('Value');
        $fixture->setSiret('Value');
        $fixture->setSiteinternet('Value');
        $fixture->setAdresse('Value');

        $$this->manager->remove($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));
        $this->client->submitForm('Delete');

        self::assertResponseRedirects('/restaurant/');
        self::assertSame(0, $this->repository->count([]));
    }
}
