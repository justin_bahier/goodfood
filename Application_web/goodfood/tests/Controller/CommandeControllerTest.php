<?php

namespace App\Test\Controller;

use App\Entity\Commande;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CommandeControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private EntityManagerInterface $manager;
    private EntityRepository $repository;
    private string $path = '/commande/';

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->manager = (static::getContainer()->get('doctrine'))->getManager();
        $this->repository = $this->manager->getRepository(Commande::class);

        foreach ($this->repository->findAll() as $object) {
            $this->manager->remove($object);
        }

        $this->manager->flush();
    }

    public function testIndex(): void
    {
        $crawler = $this->client->request('GET', $this->path);

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Commande index');

        // Use the $crawler to perform additional assertions e.g.
        // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
    }

    public function testNew(): void
    {
        $this->markTestIncomplete();
        $this->client->request('GET', sprintf('%snew', $this->path));

        self::assertResponseStatusCodeSame(200);

        $this->client->submitForm('Save', [
            'commande[adresse]' => 'Testing',
            'commande[prix]' => 'Testing',
            'commande[etats]' => 'Testing',
            'commande[idutilisateur]' => 'Testing',
            'commande[idlivreur]' => 'Testing',
            'commande[idmenu]' => 'Testing',
            'commande[idproduit]' => 'Testing',
        ]);

        self::assertResponseRedirects('/sweet/food/');

        self::assertSame(1, $this->getRepository()->count([]));
    }

    public function testShow(): void
    {
        $this->markTestIncomplete();
        $fixture = new Commande();
        $fixture->setAdresse('My Title');
        $fixture->setPrix('My Title');
        $fixture->setEtats('My Title');
        $fixture->setIdutilisateur('My Title');
        $fixture->setIdlivreur('My Title');
        $fixture->setIdmenu('My Title');
        $fixture->setIdproduit('My Title');

        $this->repository->add($fixture, true);

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Commande');

        // Use assertions to check that the properties are properly displayed.
    }

    public function testEdit(): void
    {
        $this->markTestIncomplete();
        $fixture = new Commande();
        $fixture->setAdresse('Value');
        $fixture->setPrix('Value');
        $fixture->setEtats('Value');
        $fixture->setIdutilisateur('Value');
        $fixture->setIdlivreur('Value');
        $fixture->setIdmenu('Value');
        $fixture->setIdproduit('Value');

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s/edit', $this->path, $fixture->getId()));

        $this->client->submitForm('Update', [
            'commande[adresse]' => 'Something New',
            'commande[prix]' => 'Something New',
            'commande[etats]' => 'Something New',
            'commande[idutilisateur]' => 'Something New',
            'commande[idlivreur]' => 'Something New',
            'commande[idmenu]' => 'Something New',
            'commande[idproduit]' => 'Something New',
        ]);

        self::assertResponseRedirects('/commande/');

        $fixture = $this->repository->findAll();

        self::assertSame('Something New', $fixture[0]->getAdresse());
        self::assertSame('Something New', $fixture[0]->getPrix());
        self::assertSame('Something New', $fixture[0]->getEtats());
        self::assertSame('Something New', $fixture[0]->getIdutilisateur());
        self::assertSame('Something New', $fixture[0]->getIdlivreur());
        self::assertSame('Something New', $fixture[0]->getIdmenu());
        self::assertSame('Something New', $fixture[0]->getIdproduit());
    }

    public function testRemove(): void
    {
        $this->markTestIncomplete();
        $fixture = new Commande();
        $fixture->setAdresse('Value');
        $fixture->setPrix('Value');
        $fixture->setEtats('Value');
        $fixture->setIdutilisateur('Value');
        $fixture->setIdlivreur('Value');
        $fixture->setIdmenu('Value');
        $fixture->setIdproduit('Value');

        $$this->manager->remove($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));
        $this->client->submitForm('Delete');

        self::assertResponseRedirects('/commande/');
        self::assertSame(0, $this->repository->count([]));
    }
}
