<?php

namespace App\Test\Controller;

use App\Entity\Livreur;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class LivreurControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private EntityManagerInterface $manager;
    private EntityRepository $repository;
    private string $path = '/livreur/';

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->manager = (static::getContainer()->get('doctrine'))->getManager();
        $this->repository = $this->manager->getRepository(Livreur::class);

        foreach ($this->repository->findAll() as $object) {
            $this->manager->remove($object);
        }

        $this->manager->flush();
    }

    public function testIndex(): void
    {
        $crawler = $this->client->request('GET', $this->path);

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Livreur index');

        // Use the $crawler to perform additional assertions e.g.
        // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
    }

    public function testNew(): void
    {
        $this->markTestIncomplete();
        $this->client->request('GET', sprintf('%snew', $this->path));

        self::assertResponseStatusCodeSame(200);

        $this->client->submitForm('Save', [
            'livreur[email]' => 'Testing',
            'livreur[motspasse]' => 'Testing',
            'livreur[pseudo]' => 'Testing',
            'livreur[telephone]' => 'Testing',
            'livreur[photo]' => 'Testing',
            'livreur[nom]' => 'Testing',
            'livreur[prenom]' => 'Testing',
            'livreur[rib]' => 'Testing',
            'livreur[etats]' => 'Testing',
        ]);

        self::assertResponseRedirects('/sweet/food/');

        self::assertSame(1, $this->getRepository()->count([]));
    }

    public function testShow(): void
    {
        $this->markTestIncomplete();
        $fixture = new Livreur();
        $fixture->setEmail('My Title');
        $fixture->setMotspasse('My Title');
        $fixture->setPseudo('My Title');
        $fixture->setTelephone('My Title');
        $fixture->setPhoto('My Title');
        $fixture->setNom('My Title');
        $fixture->setPrenom('My Title');
        $fixture->setRib('My Title');
        $fixture->setEtats('My Title');

        $this->repository->add($fixture, true);

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Livreur');

        // Use assertions to check that the properties are properly displayed.
    }

    public function testEdit(): void
    {
        $this->markTestIncomplete();
        $fixture = new Livreur();
        $fixture->setEmail('Value');
        $fixture->setMotspasse('Value');
        $fixture->setPseudo('Value');
        $fixture->setTelephone('Value');
        $fixture->setPhoto('Value');
        $fixture->setNom('Value');
        $fixture->setPrenom('Value');
        $fixture->setRib('Value');
        $fixture->setEtats('Value');

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s/edit', $this->path, $fixture->getId()));

        $this->client->submitForm('Update', [
            'livreur[email]' => 'Something New',
            'livreur[motspasse]' => 'Something New',
            'livreur[pseudo]' => 'Something New',
            'livreur[telephone]' => 'Something New',
            'livreur[photo]' => 'Something New',
            'livreur[nom]' => 'Something New',
            'livreur[prenom]' => 'Something New',
            'livreur[rib]' => 'Something New',
            'livreur[etats]' => 'Something New',
        ]);

        self::assertResponseRedirects('/livreur/');

        $fixture = $this->repository->findAll();

        self::assertSame('Something New', $fixture[0]->getEmail());
        self::assertSame('Something New', $fixture[0]->getMotspasse());
        self::assertSame('Something New', $fixture[0]->getPseudo());
        self::assertSame('Something New', $fixture[0]->getTelephone());
        self::assertSame('Something New', $fixture[0]->getPhoto());
        self::assertSame('Something New', $fixture[0]->getNom());
        self::assertSame('Something New', $fixture[0]->getPrenom());
        self::assertSame('Something New', $fixture[0]->getRib());
        self::assertSame('Something New', $fixture[0]->getEtats());
    }

    public function testRemove(): void
    {
        $this->markTestIncomplete();
        $fixture = new Livreur();
        $fixture->setEmail('Value');
        $fixture->setMotspasse('Value');
        $fixture->setPseudo('Value');
        $fixture->setTelephone('Value');
        $fixture->setPhoto('Value');
        $fixture->setNom('Value');
        $fixture->setPrenom('Value');
        $fixture->setRib('Value');
        $fixture->setEtats('Value');

        $$this->manager->remove($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));
        $this->client->submitForm('Delete');

        self::assertResponseRedirects('/livreur/');
        self::assertSame(0, $this->repository->count([]));
    }
}
