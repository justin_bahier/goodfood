<?php

namespace App\Test\Controller;

use App\Entity\MenuCommandeQuantite;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MenuCommandeQuantiteControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private EntityManagerInterface $manager;
    private EntityRepository $repository;
    private string $path = '/menu/commande/quantite/';

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->manager = (static::getContainer()->get('doctrine'))->getManager();
        $this->repository = $this->manager->getRepository(MenuCommandeQuantite::class);

        foreach ($this->repository->findAll() as $object) {
            $this->manager->remove($object);
        }

        $this->manager->flush();
    }

    public function testIndex(): void
    {
        $crawler = $this->client->request('GET', $this->path);

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('MenuCommandeQuantite index');

        // Use the $crawler to perform additional assertions e.g.
        // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
    }

    public function testNew(): void
    {
        $this->markTestIncomplete();
        $this->client->request('GET', sprintf('%snew', $this->path));

        self::assertResponseStatusCodeSame(200);

        $this->client->submitForm('Save', [
            'menu_commande_quantite[quantite]' => 'Testing',
            'menu_commande_quantite[idmenu]' => 'Testing',
            'menu_commande_quantite[idcommande]' => 'Testing',
        ]);

        self::assertResponseRedirects('/sweet/food/');

        self::assertSame(1, $this->getRepository()->count([]));
    }

    public function testShow(): void
    {
        $this->markTestIncomplete();
        $fixture = new MenuCommandeQuantite();
        $fixture->setQuantite('My Title');
        $fixture->setIdmenu('My Title');
        $fixture->setIdcommande('My Title');

        $this->repository->add($fixture, true);

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('MenuCommandeQuantite');

        // Use assertions to check that the properties are properly displayed.
    }

    public function testEdit(): void
    {
        $this->markTestIncomplete();
        $fixture = new MenuCommandeQuantite();
        $fixture->setQuantite('Value');
        $fixture->setIdmenu('Value');
        $fixture->setIdcommande('Value');

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s/edit', $this->path, $fixture->getId()));

        $this->client->submitForm('Update', [
            'menu_commande_quantite[quantite]' => 'Something New',
            'menu_commande_quantite[idmenu]' => 'Something New',
            'menu_commande_quantite[idcommande]' => 'Something New',
        ]);

        self::assertResponseRedirects('/menu/commande/quantite/');

        $fixture = $this->repository->findAll();

        self::assertSame('Something New', $fixture[0]->getQuantite());
        self::assertSame('Something New', $fixture[0]->getIdmenu());
        self::assertSame('Something New', $fixture[0]->getIdcommande());
    }

    public function testRemove(): void
    {
        $this->markTestIncomplete();
        $fixture = new MenuCommandeQuantite();
        $fixture->setQuantite('Value');
        $fixture->setIdmenu('Value');
        $fixture->setIdcommande('Value');

        $$this->manager->remove($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));
        $this->client->submitForm('Delete');

        self::assertResponseRedirects('/menu/commande/quantite/');
        self::assertSame(0, $this->repository->count([]));
    }
}
