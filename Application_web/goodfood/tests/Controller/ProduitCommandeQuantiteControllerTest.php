<?php

namespace App\Test\Controller;

use App\Entity\ProduitCommandeQuantite;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProduitCommandeQuantiteControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private EntityManagerInterface $manager;
    private EntityRepository $repository;
    private string $path = '/produit/commande/quantite/';

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->manager = (static::getContainer()->get('doctrine'))->getManager();
        $this->repository = $this->manager->getRepository(ProduitCommandeQuantite::class);

        foreach ($this->repository->findAll() as $object) {
            $this->manager->remove($object);
        }

        $this->manager->flush();
    }

    public function testIndex(): void
    {
        $crawler = $this->client->request('GET', $this->path);

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('ProduitCommandeQuantite index');

        // Use the $crawler to perform additional assertions e.g.
        // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
    }

    public function testNew(): void
    {
        $this->markTestIncomplete();
        $this->client->request('GET', sprintf('%snew', $this->path));

        self::assertResponseStatusCodeSame(200);

        $this->client->submitForm('Save', [
            'produit_commande_quantite[quantite]' => 'Testing',
            'produit_commande_quantite[idproduit]' => 'Testing',
            'produit_commande_quantite[idcommande]' => 'Testing',
        ]);

        self::assertResponseRedirects('/sweet/food/');

        self::assertSame(1, $this->getRepository()->count([]));
    }

    public function testShow(): void
    {
        $this->markTestIncomplete();
        $fixture = new ProduitCommandeQuantite();
        $fixture->setQuantite('My Title');
        $fixture->setIdproduit('My Title');
        $fixture->setIdcommande('My Title');

        $this->repository->add($fixture, true);

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('ProduitCommandeQuantite');

        // Use assertions to check that the properties are properly displayed.
    }

    public function testEdit(): void
    {
        $this->markTestIncomplete();
        $fixture = new ProduitCommandeQuantite();
        $fixture->setQuantite('Value');
        $fixture->setIdproduit('Value');
        $fixture->setIdcommande('Value');

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s/edit', $this->path, $fixture->getId()));

        $this->client->submitForm('Update', [
            'produit_commande_quantite[quantite]' => 'Something New',
            'produit_commande_quantite[idproduit]' => 'Something New',
            'produit_commande_quantite[idcommande]' => 'Something New',
        ]);

        self::assertResponseRedirects('/produit/commande/quantite/');

        $fixture = $this->repository->findAll();

        self::assertSame('Something New', $fixture[0]->getQuantite());
        self::assertSame('Something New', $fixture[0]->getIdproduit());
        self::assertSame('Something New', $fixture[0]->getIdcommande());
    }

    public function testRemove(): void
    {
        $this->markTestIncomplete();
        $fixture = new ProduitCommandeQuantite();
        $fixture->setQuantite('Value');
        $fixture->setIdproduit('Value');
        $fixture->setIdcommande('Value');

        $$this->manager->remove($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));
        $this->client->submitForm('Delete');

        self::assertResponseRedirects('/produit/commande/quantite/');
        self::assertSame(0, $this->repository->count([]));
    }
}
