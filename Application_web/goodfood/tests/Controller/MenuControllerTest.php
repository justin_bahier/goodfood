<?php

namespace App\Test\Controller;

use App\Entity\Menu;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MenuControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private EntityManagerInterface $manager;
    private EntityRepository $repository;
    private string $path = '/menu/';

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->manager = (static::getContainer()->get('doctrine'))->getManager();
        $this->repository = $this->manager->getRepository(Menu::class);

        foreach ($this->repository->findAll() as $object) {
            $this->manager->remove($object);
        }

        $this->manager->flush();
    }

    public function testIndex(): void
    {
        $crawler = $this->client->request('GET', $this->path);

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Menu index');

        // Use the $crawler to perform additional assertions e.g.
        // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
    }

    public function testNew(): void
    {
        $this->markTestIncomplete();
        $this->client->request('GET', sprintf('%snew', $this->path));

        self::assertResponseStatusCodeSame(200);

        $this->client->submitForm('Save', [
            'menu[nom]' => 'Testing',
            'menu[description]' => 'Testing',
            'menu[prix]' => 'Testing',
            'menu[photo]' => 'Testing',
            'menu[quantité]' => 'Testing',
            'menu[idcommande]' => 'Testing',
            'menu[idproduit]' => 'Testing',
            'menu[idrestaurant]' => 'Testing',
        ]);

        self::assertResponseRedirects('/sweet/food/');

        self::assertSame(1, $this->getRepository()->count([]));
    }

    public function testShow(): void
    {
        $this->markTestIncomplete();
        $fixture = new Menu();
        $fixture->setNom('My Title');
        $fixture->setDescription('My Title');
        $fixture->setPrix('My Title');
        $fixture->setPhoto('My Title');
        $fixture->setQuantité('My Title');
        $fixture->setIdcommande('My Title');
        $fixture->setIdproduit('My Title');
        $fixture->setIdrestaurant('My Title');

        $this->repository->add($fixture, true);

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Menu');

        // Use assertions to check that the properties are properly displayed.
    }

    public function testEdit(): void
    {
        $this->markTestIncomplete();
        $fixture = new Menu();
        $fixture->setNom('Value');
        $fixture->setDescription('Value');
        $fixture->setPrix('Value');
        $fixture->setPhoto('Value');
        $fixture->setQuantité('Value');
        $fixture->setIdcommande('Value');
        $fixture->setIdproduit('Value');
        $fixture->setIdrestaurant('Value');

        $this->manager->persist($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s/edit', $this->path, $fixture->getId()));

        $this->client->submitForm('Update', [
            'menu[nom]' => 'Something New',
            'menu[description]' => 'Something New',
            'menu[prix]' => 'Something New',
            'menu[photo]' => 'Something New',
            'menu[quantité]' => 'Something New',
            'menu[idcommande]' => 'Something New',
            'menu[idproduit]' => 'Something New',
            'menu[idrestaurant]' => 'Something New',
        ]);

        self::assertResponseRedirects('/menu/');

        $fixture = $this->repository->findAll();

        self::assertSame('Something New', $fixture[0]->getNom());
        self::assertSame('Something New', $fixture[0]->getDescription());
        self::assertSame('Something New', $fixture[0]->getPrix());
        self::assertSame('Something New', $fixture[0]->getPhoto());
        self::assertSame('Something New', $fixture[0]->getQuantité());
        self::assertSame('Something New', $fixture[0]->getIdcommande());
        self::assertSame('Something New', $fixture[0]->getIdproduit());
        self::assertSame('Something New', $fixture[0]->getIdrestaurant());
    }

    public function testRemove(): void
    {
        $this->markTestIncomplete();
        $fixture = new Menu();
        $fixture->setNom('Value');
        $fixture->setDescription('Value');
        $fixture->setPrix('Value');
        $fixture->setPhoto('Value');
        $fixture->setQuantité('Value');
        $fixture->setIdcommande('Value');
        $fixture->setIdproduit('Value');
        $fixture->setIdrestaurant('Value');

        $$this->manager->remove($fixture);
        $this->manager->flush();

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));
        $this->client->submitForm('Delete');

        self::assertResponseRedirects('/menu/');
        self::assertSame(0, $this->repository->count([]));
    }
}
