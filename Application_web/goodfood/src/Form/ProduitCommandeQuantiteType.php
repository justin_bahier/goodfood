<?php

namespace App\Form;
use App\Entity\Produit;
use App\Entity\Commande;
use App\Entity\ProduitCommandeQuantite;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ProduitCommandeQuantiteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('quantite')
            ->add('idcommande', EntityType::class, [
                // looks for choices from this entity
                'class' => Commande::class,
            
                // uses the User.username property as the visible option string
                'choice_label' => 'adresse',
                // used to render a select box, check boxes or radios
                 //'multiple' => true,
                // 'expanded' => true,
            ])
            ->add('idproduit', EntityType::class, [
                // looks for choices from this entity
                'class' => Produit::class,
            
                // uses the User.username property as the visible option string
                'choice_label' => 'nom',
                // used to render a select box, check boxes or radios
                // 'multiple' => true,
                // 'expanded' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ProduitCommandeQuantite::class,
        ]);
    }
}
