<?php

namespace App\Form;

use App\Entity\MenuCommandeQuantite;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Menu;
use App\Entity\Commande;

class MenuCommandeQuantiteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('quantite')
            ->add('idcommande', EntityType::class, [
                // looks for choices from this entity
                'class' => Commande::class,
            
                // uses the User.username property as the visible option string
                'choice_label' => 'adresse',
                // used to render a select box, check boxes or radios
                 //'multiple' => true,
                // 'expanded' => true,
            ])
            ->add('idmenu', EntityType::class, [
                // looks for choices from this entity
                'class' => Menu::class,
            
                // uses the User.username property as the visible option string
                'choice_label' => 'nom',
                // used to render a select box, check boxes or radios
                // 'multiple' => true,
                // 'expanded' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => MenuCommandeQuantite::class,
        ]);
    }
}
