<?php

namespace App\Form;

use App\Entity\Commande;
use App\Entity\Livreur;
use App\Entity\Produit;
use App\Entity\Utilisateur;
use App\Entity\Menu;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class CommandeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('adresse')
            ->add('prix')
            ->add('etats')
            
            ->add('idproduit', EntityType::class, [
                // looks for choices from this entity
                'class' => Produit::class,
            
                // uses the User.username property as the visible option string
                'choice_label' => 'nom',
                // used to render a select box, check boxes or radios
                 'multiple' => true,
                // 'expanded' => true,
            ])
            ->add('idmenu', EntityType::class, [
                // looks for choices from this entity
                'class' => Menu::class,
            
                // uses the User.username property as the visible option string
                'choice_label' => 'nom',
                // used to render a select box, check boxes or radios
                 'multiple' => true,
                // 'expanded' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Commande::class,
        ]);
    }
}
