<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Restaurant;
use App\Entity\Menu;
use \Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * Menu
 *
 * @ORM\Table(name="MenuCommandeQuantite", 
 * indexes={})
 * @ORM\Entity
   * @ApiResource()
 */ class MenuCommandeQuantite
{
    /**
     * @var int
     *
     * @ORM\Column(name="idMenuCommandeQuantite", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idMenuCommandeQuantite;




    /**
     * @var int
     *
     * @ORM\Column(name="Quantite", type="integer", nullable=false)
     */
    private $quantite;

    /**
     * @ORM\ManyToOne(targetEntity=Menu::class, inversedBy="idMenuCommandeQuantite")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="IdMenu")
     */
    private $idmenu;

    /**
     * @ORM\ManyToOne(targetEntity=Commande::class, inversedBy="idMenuCommandeQuantite")
     * @ORM\JoinColumn(nullable=false , referencedColumnName="IdCommande")
     */
    private $idcommande;


    public function IncrementationQuantite(): self
    {
        $this->quantite = $this->quantite + 1;

        return $this;
    }
    public function DecrementationQuantite(): self
    {
        if ($this->quantite > 0) {
            $this->quantite = $this->quantite - 1;
        }
        return $this;
    }
    public function __construct()
    {
        $this->quantite = 0;
        $this->menuCommandeQuantites = new ArrayCollection();
    }

    public function getIdMenuCommandeQuantite(): ?int
    {
        return $this->idMenuCommandeQuantite;
    }

    public function getQuantite(): ?int
    {
        return $this->quantite;
    }
    public function setQuantite(?int $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    public function getIdmenu(): ?Menu
    {
        return $this->idmenu;
    }

    public function setIdmenu(?Menu $idmenu): self
    {
        $this->idmenu = $idmenu;

        return $this;
    }

    public function getIdcommande(): ?Commande
    {
        return $this->idcommande;
    }

    public function setIdcommande(?Commande $idcommande): self
    {
        $this->idcommande = $idcommande;

        return $this;
    }
}
