<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\Collection ;
use App\Entity\Livreur;
use App\Entity\Utilisateur;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiProperty;

/**
 * Commande
 *
 * @ORM\Table(name="commande", indexes={@ORM\Index(name="Commande_Utilisateur_FK", columns={"IdUtilisateur"}), @ORM\Index(name="Commande_Livreur0_FK", columns={"IdLivreur"})})
 * @ORM\Entity
 * @ApiResource(graphql={
 *          "item_query",
 *          "collection_query"
 *     })
 */
class Commande
{
    /**
     * @var int
     *
     * @ORM\Column(name="IdCommande", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ApiProperty(identifier=true)
     */
    private $idcommande;

    /**
     * @var string
     *
     * @ORM\Column(name="Adresse", type="string", length=30, nullable=false)
     */
    private $adresse;

    /**
     * @var int
     *
     * @ORM\Column(name="Prix", type="integer", nullable=false)
     */
    private $prix;

    /**
     * @var string
     *
     * @ORM\Column(name="Etats", type="string", length=50, nullable=false)
     */
    private $etats;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="IdUtilisateur", referencedColumnName="IdUtilisateur")
     * })
     */
    private $idutilisateur;

    /**
     * @var \Livreur
     *
     * @ORM\ManyToOne(targetEntity="Livreur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="IdLivreur", referencedColumnName="IdLivreur")
     * })
     */
    private $idlivreur;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Menu", inversedBy="idcommande")
     * @ORM\JoinTable(name="menucommande",
     *   joinColumns={
     *     @ORM\JoinColumn(name="IdCommande", referencedColumnName="IdCommande")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="IdMenu", referencedColumnName="IdMenu")
     *   }
     * )
     */
    private $idmenu;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Produit", inversedBy="idcommande")
     * @ORM\JoinTable(name="produitcommande",
     *   joinColumns={
     *     @ORM\JoinColumn(name="IdCommande", referencedColumnName="IdCommande")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="IdProduit", referencedColumnName="IdProduit")
     *   }
     * )
     */
    private $idproduit;

  

  /**
   * @ORM\OneToMany(targetEntity=MenuCommandeQuantite::class, mappedBy="idcommande")
   */
  private $idMenuCommandeQuantite;


    /**
   * @ORM\OneToMany(targetEntity=ProduitCommandeQuantite::class, mappedBy="idcommande")
   */
  private $idProduitCommandeQuantite;

    public function getPrix(): ?int
    {
        return $this->prix;
    }
    public function setPrix(?int $prix): self
    {
        $this->prix = $prix;

        return $this;
    }    
    
    public function getEtats(): ?string
    {
        return $this->etats;
    }
    public function setEtats(?string $etats): self
    {
        $this->etats = $etats;

        return $this;
    }
    public function getAdresse(): ?string
    {
        return $this->adresse;
    }
    public function setAdresse(?string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }
    
    public function getIdcommande(): ?int
    {
        return $this->idcommande;
    }

    public function getIdutilisateur(): ?Utilisateur
    {
        return $this->idutilisateur;
    }
    public function setIdutilisateur(?Utilisateur $idutilisateur): self
    {
        $this->idutilisateur = $idutilisateur;

        return $this;
    }
    public function getIdlivreur(): ?Livreur
    {
        return $this->idlivreur;
    }
    public function setIdlivreur(?Livreur $idlivreur): self
    {
        $this->idlivreur = $idlivreur;

        return $this;
    }

    public function getIdmenu(): ?Collection
    {
        return $this->idmenu;
    }
    public function setIdmenu(?string $idmenu): self
    {
        $this->idmenu = $idmenu;

        return $this;
    }



    public function addProduitCommandeQuantite(ProduitCommandeQuantite $ProduitCommandeQuantite): self
    {
        $this->ProduitCommandeQuantite[] = $ProduitCommandeQuantite;
 
        return $this;
    }
 
    public function removeProduitCommandeQuantite(ProduitCommandeQuantite $ProduitCommandeQuantite): bool
    {
        return $this->ProduitCommandeQuantite->removeElement($ProduitCommandeQuantite);
    }
 
    public function getProduitCommandeQuantite(): Collection
    {
        return $this->idproduit;
    }
    public function setProduitCommandeQuantite(?ProduitCommandeQuantite $ProduitCommandeQuantite): self
    {
        $this->ProduitCommandeQuantite = $ProduitCommandeQuantite;

        return $this;
    }




    public function addIdproduit(Produit $idproduit): self
    {
        $this->idproduit[] = $idproduit;
 
        return $this;
    }
 
    public function removeIdproduit(Produit $idproduit): bool
    {
        return $this->idproduit->removeElement($idproduit);
    }
 
    public function getIdproduit(): Collection
    {
        return $this->idproduit;
    }
    public function setIdproduit(?Produit $idproduit): self
    {
        $this->idproduit = $idproduit;

        return $this;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idmenu = new \Doctrine\Common\Collections\ArrayCollection();
        $this->idproduit = new \Doctrine\Common\Collections\ArrayCollection();
        $this->idProduitCommandeQuantite = new ArrayCollection();
        $this->idMenuCommandeQuantite = new ArrayCollection();

    }

    /**
     * @return Collection<int, ProduitCommandeQuantite>
     */
    public function getIdProduitCommandeQuantite(): Collection
    {
        return $this->idProduitCommandeQuantite;
    }

    public function addIdProduitCommandeQuantite(ProduitCommandeQuantite $idProduitCommandeQuantite): self
    {
        if (!$this->idProduitCommandeQuantite->contains($idProduitCommandeQuantite)) {
            $this->idProduitCommandeQuantite[] = $idProduitCommandeQuantite;
            $idProduitCommandeQuantite->setIdcommande($this);
        }

        return $this;
    }

    public function removeIdProduitCommandeQuantite(ProduitCommandeQuantite $idProduitCommandeQuantite): self
    {
        if ($this->idProduitCommandeQuantite->removeElement($idProduitCommandeQuantite)) {
            // set the owning side to null (unless already changed)
            if ($idProduitCommandeQuantite->getIdcommande() === $this) {
                $idProduitCommandeQuantite->setIdcommande(null);
            }
        }

        return $this;
    }
    /**
     * @return Collection<int, MenuCommandeQuantite>
     */
    public function getIdMenuCommandeQuantite(): Collection
    {
        return $this->idMenuCommandeQuantite;
    }

    public function addIdMenuCommandeQuantite(MenuCommandeQuantite $idMenuCommandeQuantite): self
    {
        if (!$this->idMenuCommandeQuantite->contains($idMenuCommandeQuantite)) {
            $this->idMenuCommandeQuantite[] = $idMenuCommandeQuantite;
            $idMenuCommandeQuantite->setIdcommande($this);
        }

        return $this;
    }

    public function removeIdMenuCommandeQuantite(MenuCommandeQuantite $idMenuCommandeQuantite): self
    {
        if ($this->idMenuCommandeQuantite->removeElement($idMenuCommandeQuantite)) {
            // set the owning side to null (unless already changed)
            if ($idMenuCommandeQuantite->getIdcommande() === $this) {
                $idMenuCommandeQuantite->setIdcommande(null);
            }
        }

        return $this;
    }
}
