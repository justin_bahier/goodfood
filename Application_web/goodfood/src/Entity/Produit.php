<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Restaurant;
use App\Entity\Menu;
use \Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * Produit
 *
 * @ORM\Table(name="produit", indexes={@ORM\Index(name="Produit_Restaurant_FK", columns={"IdRestaurant"})})
 * @ORM\Entity
   * @ApiResource()
 */
class Produit
{
    /**
     * @var int
     *
     * @ORM\Column(name="IdProduit", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idproduit;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom", type="string", length=30, nullable=false)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="Description", type="string", length=250, nullable=false)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateCreation", type="date", nullable=false)
     */
    private $datecreation;

    /**
     * @var int
     *
     * @ORM\Column(name="Prix", type="integer", nullable=false)
     */
    private $prix;

    /**
     * @var string
     *
     * @ORM\Column(name="Photo", type="string", length=50, nullable=false)
     */
    private $photo;

    /**
     * @var \Restaurant
     *
     * @ORM\ManyToOne(targetEntity="Restaurant")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="IdRestaurant", referencedColumnName="IdRestaurant")
     * })
     */
    private $idrestaurant;

    /**
     * @var \Menu
     *
     * @ORM\ManyToMany(targetEntity="Menu", mappedBy="idproduit")
     */
    private $idmenu;

    /**
     * @var int
     *
     * @ORM\Column(name="Quantite", type="integer", nullable=false)
     */
    private $quantite;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Commande", mappedBy="idproduit")
     */
    private $idcommande;

    /**
     * @ORM\OneToMany(targetEntity=ProduitCommandeQuantite::class, mappedBy="idproduit")
     */
    private $idProduitCommandeQuantite;



    public function getQuantite(): ?int
    {
        return $this->quantite;
    }
    public function setQuantite(?int $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }
    public function IncrementationQuantite(): self
    {
        $this->quantite = $this->quantite + 1;

        return $this;
    }
    public function getIdproduit(): ?string
    {
        return $this->idproduit;
    }
    public function getNom(): ?string
    {
        return $this->nom;
    }
    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }
    public function getDatecreation(): \DateTime
    {
        return $this->datecreation;
    }
    public function setDatecreation(\DateTime $datecreation): self
    {
        $this->datecreation = $datecreation;

        return $this;
    }

    public function getPrix(): ?int
    {
        return $this->prix;
    }
    public function setPrix(?int $prix): self
    {
        $this->prix = $prix;

        return $this;
    }
    public function getPhoto(): ?string
    {
        return $this->photo;
    }
    public function getPhotoAvecChemin(): ?string
    {
        return 'Images/' . $this->photo;
    }
    public function setPhoto(?string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }
    public function getIdrestaurant(): ?Restaurant
    {
        return $this->idrestaurant;
    }
    public function getIdmenu(): ?Menu
    {
        return $this->idmenu;
    }

    public function setIdcommande(?Collection $idcommande): self
    {
        $this->idcommande = $idcommande;

        return $this;
    }
    public function getIdcommande(): ?Collection
    {
        return $this->idcommande;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idmenu = new \Doctrine\Common\Collections\ArrayCollection();
        $this->idcommande = new \Doctrine\Common\Collections\ArrayCollection();
        $this->idProduitCommandeQuantite = new ArrayCollection();
    }

    /**
     * @return Collection<int, ProduitCommandeQuantite>
     */
    public function getIdProduitCommandeQuantite(): Collection
    {
        return $this->idProduitCommandeQuantite;
    }

    public function addIdProduitCommandeQuantite(ProduitCommandeQuantite $idProduitCommandeQuantite): self
    {
        if (!$this->idProduitCommandeQuantite->contains($idProduitCommandeQuantite)) {
            $this->idProduitCommandeQuantite[] = $idProduitCommandeQuantite;
            $idProduitCommandeQuantite->setIdProduit($this);
        }

        return $this;
    }

    public function removeIdProduitCommandeQuantite(ProduitCommandeQuantite $idProduitCommandeQuantite): self
    {
        if ($this->idProduitCommandeQuantite->removeElement($idProduitCommandeQuantite)) {
            // set the owning side to null (unless already changed)
            if ($idProduitCommandeQuantite->getIdProduit() === $this) {
                $idProduitCommandeQuantite->setIdProduit(null);
            }
        }

        return $this;
    }
}
