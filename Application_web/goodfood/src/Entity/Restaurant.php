<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * Restaurant
 *
 * @ORM\Table(name="restaurant")
 * @ORM\Entity
   * @ApiResource()
 */
class Restaurant
{
    /**
     * @var int
     *
     * @ORM\Column(name="IdRestaurant", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idrestaurant;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom", type="string", length=30, nullable=false)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="Description", type="string", length=250, nullable=false)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="Siret", type="string", length=30, nullable=false)
     */
    private $siret;

    /**
     * @var string
     *
     * @ORM\Column(name="SiteInternet", type="string", length=30, nullable=false)
     */
    private $siteinternet;

    /**
     * @var string
     *
     * @ORM\Column(name="Adresse", type="string", length=30, nullable=false)
     */
    private $adresse;

    private $ListeProduit;

    private $ListeMenu;
    
    public function setListeProduit(?string $ListeProduit): self
    {
        $this->ListeProduit = $ListeProduit;

        return $this;
    }
    public function getListeProduit(): ?int
    {
        return $this->ListeProduit;

    }

    public function getIdrestaurant(): ?int
    {
        return $this->idrestaurant;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }
    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }


    public function getSiret(): ?string
    {
        return $this->siret;
    }
    public function setSiret(?string $siret): self
    {
        $this->siret = $siret;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }
    public function setAdresse(?string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getSiteinternet(): ?string
    {
        return $this->siteinternet;
    }
    public function setSiteinternet(?string $siteinternet): self
    {
        $this->siteinternet = $siteinternet;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

}
