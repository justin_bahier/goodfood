<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Restaurant;
use App\Entity\Menu;
use \Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * Produit
 *
 * @ORM\Table(name="ProduitCommandeQuantite", 
 * indexes={})
 * @ORM\Entity
   * @ApiResource()
 */ class ProduitCommandeQuantite
{
    /**
     * @var int
     *
     * @ORM\Column(name="idProduitCommandeQuantite", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idProduitCommandeQuantite;




    /**
     * @var int
     *
     * @ORM\Column(name="Quantite", type="integer", nullable=false)
     */
    private $quantite;

    /**
     * @ORM\ManyToOne(targetEntity=Produit::class, inversedBy="idProduitCommandeQuantite")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="IdProduit")
     */
    private $idproduit;

    /**
     * @ORM\ManyToOne(targetEntity=Commande::class, inversedBy="idProduitCommandeQuantite")
     * @ORM\JoinColumn(nullable=false , referencedColumnName="IdCommande")
     */
    private $idcommande;


    public function IncrementationQuantite(): self
    {
        $this->quantite = $this->quantite + 1;

        return $this;
    }
    public function DecrementationQuantite(): self
    {
        if ($this->quantite > 0) {
            $this->quantite = $this->quantite - 1;
        }
        return $this;
    }
    public function __construct()
    {
        $this->quantite = 0;
        $this->produitCommandeQuantites = new ArrayCollection();
    }

    public function getIdProduitCommandeQuantite(): ?int
    {
        return $this->idProduitCommandeQuantite;
    }

    public function getQuantite(): ?int
    {
        return $this->quantite;
    }
    public function setQuantite(?int $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    public function getIdproduit(): ?Produit
    {
        return $this->idproduit;
    }

    public function setIdproduit(?Produit $idproduit): self
    {
        $this->idproduit = $idproduit;

        return $this;
    }

    public function getIdcommande(): ?Commande
    {
        return $this->idcommande;
    }

    public function setIdcommande(?Commande $idcommande): self
    {
        $this->idcommande = $idcommande;

        return $this;
    }
}
