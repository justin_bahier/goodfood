<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Restaurant;
use App\Entity\Produit;
use App\Entity\Commande;
use \Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * Menu
 *
 * @ORM\Table(name="menu")
 * @ORM\Entity
   * @ApiResource()
 */
class Menu
{
    /**
     * @var int
     *
     * @ORM\Column(name="IdMenu", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idmenu;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom", type="string", length=50, nullable=false)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="Description", type="string", length=50, nullable=false)
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="Prix", type="integer", nullable=false)
     */
    private $prix;

    /**
     * @var string
     *
     * @ORM\Column(name="Photo", type="string", length=50, nullable=false)
     */
    private $photo;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Commande", mappedBy="idmenu")
     */
    private $idcommande;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Produit", inversedBy="idmenu")
     * @ORM\JoinTable(name="menuproduit",
     *   joinColumns={
     *     @ORM\JoinColumn(name="IdMenu", referencedColumnName="IdMenu")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="IdProduit", referencedColumnName="IdProduit")
     *   }
     * )
     */
    private $idproduit;

        /**
     * @var \Restaurant
     *
     * @ORM\ManyToOne(targetEntity="Restaurant")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="IdRestaurant", referencedColumnName="IdRestaurant")
     * })
     */
    private $idrestaurant;
     /**
     * @var int
     *
     * @ORM\Column(name="Quantite", type="integer", nullable=false)
     */
    private $quantite;

    /**
     * @ORM\OneToMany(targetEntity=MenuCommandeQuantite::class, mappedBy="idproduit")
     */
    private $idMenuCommandeQuantite;

    public function getQuantite(): ?int
    {
        return $this->quantite;
    }
    public function setQuantite(?int $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }
    public function getIdmenu(): ?int
    {
        return $this->idmenu;
    }
    public function getNom(): ?string
    {
        return $this->nom;
    }
    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPrix(): ?int
    {
        return $this->prix;
    }
    public function setPrix(?int $prix): self
    {
        $this->prix = $prix;

        return $this;
    }
    public function getPhoto(): ?string
    {
        return $this->photo;
    }
    public function getPhotoAvecChemin(): ?string
    {
        return 'Images/'.$this->photo;
    }
    public function setPhoto(?string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }
    public function getIdrestaurant(): ?Restaurant
    {
        return $this->idrestaurant;
    }
    public function getIdproduit(): ?Produit
    {
        return $this->idproduit;
    }
    public function getIdcommande(): ?Commande
    {
        return $this->idcommande;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idcommande = new \Doctrine\Common\Collections\ArrayCollection();
        $this->idproduit = new \Doctrine\Common\Collections\ArrayCollection();
        $this->idMenuCommandeQuantite = new ArrayCollection();

    }
    /**
     * @return Collection<int, MenuCommandeQuantite>
     */
    public function getIdMenuCommandeQuantite(): Collection
    {
        return $this->idMenuCommandeQuantite;
    }

    public function addIdMenuCommandeQuantite(MenuCommandeQuantite $idMenuCommandeQuantite): self
    {
        if (!$this->idMenuCommandeQuantite->contains($idMenuCommandeQuantite)) {
            $this->idMenuCommandeQuantite[] = $idMenuCommandeQuantite;
            $idMenuCommandeQuantite->setIdMenu($this);
        }

        return $this;
    }

    public function removeIdMenuCommandeQuantite(MenuCommandeQuantite $idMenuCommandeQuantite): self
    {
        if ($this->idMenuCommandeQuantite->removeElement($idMenuCommandeQuantite)) {
            // set the owning side to null (unless already changed)
            if ($idMenuCommandeQuantite->getIdMenu() === $this) {
                $idMenuCommandeQuantite->setIdMenu(null);
            }
        }

        return $this;
    }
}
