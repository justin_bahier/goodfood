<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * Livreur
 *
 * @ORM\Table(name="livreur")
 * @ORM\Entity
  * @ApiResource()
 */
class Livreur
{
    /**
     * @var int
     *
     * @ORM\Column(name="IdLivreur", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idlivreur;

    /**
     * @var string
     *
     * @ORM\Column(name="Email", type="string", length=30, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="MotsPasse", type="string", length=30, nullable=false)
     */
    private $motspasse;

    /**
     * @var string
     *
     * @ORM\Column(name="Pseudo", type="string", length=30, nullable=false)
     */
    private $pseudo;

    /**
     * @var string
     *
     * @ORM\Column(name="Telephone", type="string", length=30, nullable=false)
     */
    private $telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="Photo", type="string", length=100, nullable=false)
     */
    private $photo;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom", type="string", length=30, nullable=false)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="Prenom", type="string", length=30, nullable=false)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="RIB", type="string", length=30, nullable=false)
     */
    private $rib;

    /**
     * @var string
     *
     * @ORM\Column(name="Etats", type="string", length=50, nullable=false)
     */
    private $etats;

    public function getEtats(): ?string
    {
        return $this->etats;
    }
    public function setEtats(?string $etats): self
    {
        $this->etats = $etats;

        return $this;
    }
    public function getIdlivreur(): ?int
    {
        return $this->idlivreur;
    }
    public function getRib(): ?string
    {
        return $this->rib;
    }
    public function setRib(?string $rib): self
    {
        $this->rib = $rib;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }
    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }
    public function getEmail(): ?string
    {
        return $this->email;
    }
    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }
    public function setTelephone(?string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }
    public function setPseudo(?string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }
    
    public function getPhoto(): ?string
    {
        return $this->photo;
    }
    public function setPhoto(?string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }
    public function setPrenom(?string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }
}
