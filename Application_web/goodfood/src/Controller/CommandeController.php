<?php

namespace App\Controller;

use App\Entity\Produit;
use App\Entity\Menu;

use App\Entity\Commande;
use App\Form\CommandeType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use App\Entity\ProduitCommandeQuantite;
use App\Form\ProduitCommandeQuantiteType;
use App\Entity\MenuCommandeQuantite;
use App\Form\MenuCommandeQuantiteType;

#[Route('/commande')]
class CommandeController extends AbstractController
{
    private $security;
    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    #[Route('/', name: 'app_commande_index', methods: ['GET'])]
    public function index(EntityManagerInterface $entityManager): Response
    {
        $commandes = $entityManager
            ->getRepository(Commande::class)
            ->findAll();

        return $this->render('commande/index.html.twig', [
            'commandes' => $commandes,
        ]);
    }

    #[Route('/new', name: 'app_commande_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $commande = new Commande();
        $form = $this->createForm(CommandeType::class, $commande);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($commande);
            $entityManager->flush();

            return $this->redirectToRoute('app_commande_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('commande/new.html.twig', [
            'commande' => $commande,
            'form' => $form,
        ]);
    }
    #[Route('/EnCours', name: 'app_commande_EnCours', methods: ['GET'])]
    public function showEnCours(EntityManagerInterface $entityManager): Response
    {



        $User = $this->security->getUser();

        $idutilisateur =  $User->getIdutilisateur()->getIdutilisateur();
        $idutilisateurString =   '' . $idutilisateur;
        $Commande = $entityManager->getRepository(Commande::class)->findOneBy(array('idutilisateur' => $idutilisateurString, 'etats' => 'EnCours'), ['idcommande' => 'DESC']);
        if ($Commande == null) {
            return $this->render('commande/PasDeCommandeEnCours.html.twig', []);

        } else {
            $prixtotal = 0;

            foreach ($Commande->getIdProduitCommandeQuantite() as $ProduitCommandeQuantite) {
                $prixtotal = $prixtotal + ($ProduitCommandeQuantite->getIdproduit()->getPrix() * $ProduitCommandeQuantite->getQuantite());
            }

            foreach ($Commande->getIdMenuCommandeQuantite() as $MenuCommandeQuantite) {
                $prixtotal = $prixtotal + ($MenuCommandeQuantite->getIdmenu()->getPrix() * $MenuCommandeQuantite->getQuantite());
            }


            return $this->render('commande/showEnCours.html.twig', [
                'commande' => $Commande,
                'prixtotal' => $prixtotal,

            ]);
        }
    }
    /**
     * @Route("/{idProduitCommandeQuantite}/quantiteDecrementation", name="app_produit_commande_quantite_Decrementation", methods={"GET", "POST"})
     */
    public function quantiteDecrementation(Request $request, ProduitCommandeQuantite $produitCommandeQuantite, EntityManagerInterface $entityManager): Response
    {


        $produitCommandeQuantite->DecrementationQuantite();
        $form = $this->createForm(ProduitCommandeQuantiteType::class, $produitCommandeQuantite);
        $form->handleRequest($request);

        $entityManager->flush();





        return $this->redirectToRoute('app_commande_EnCours', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route("/{idMenuCommandeQuantite}/quantiteDecrementationMenu", name="app_menu_commande_quantite_Decrementation", methods={"GET", "POST"})
     */
    public function quantiteDecrementationMenu(Request $request, MenuCommandeQuantite $menuCommandeQuantite, EntityManagerInterface $entityManager): Response
    {
        $menuCommandeQuantite->DecrementationQuantite();
        $form = $this->createForm(MenuCommandeQuantiteType::class, $menuCommandeQuantite);
        $form->handleRequest($request);

        $entityManager->flush();

        return $this->redirectToRoute('app_commande_EnCours', [], Response::HTTP_SEE_OTHER);
    }
    #[Route('/{idcommande}', name: 'app_commande_show', methods: ['GET'])]
    public function show(Commande $commande): Response
    {
        return $this->render('commande/show.html.twig', [
            'commande' => $commande,
        ]);
    }




    #[Route('/{idcommande}/edit', name: 'app_commande_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Commande $commande, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(CommandeType::class, $commande);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_commande_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('commande/edit.html.twig', [
            'commande' => $commande,
            'form' => $form,
        ]);
    }

    #[Route('/{idcommande}', name: 'app_commande_delete', methods: ['POST'])]
    public function delete(Request $request, Commande $commande, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete' . $commande->getIdcommande(), $request->request->get('_token'))) {
            $User = $this->security->getUser();

            $idutilisateur =  $User->getIdutilisateur()->getIdutilisateur();
            $idutilisateurString =   '' . $idutilisateur;
            $Commande = $entityManager->getRepository(Commande::class)->findOneBy(array('idutilisateur' => $idutilisateurString, 'etats' => 'EnCours'), ['idcommande' => 'DESC']);
            $Commande->setEtats('Annulé');

            $entityManager->persist($Commande);
            $entityManager->flush();
            

        }

        return $this->redirectToRoute('app_liste_produit', [], Response::HTTP_SEE_OTHER);
    }
}
