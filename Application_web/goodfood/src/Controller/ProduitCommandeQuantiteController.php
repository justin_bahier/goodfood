<?php

namespace App\Controller;

use App\Entity\ProduitCommandeQuantite;
use App\Form\ProduitCommandeQuantiteType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/produit/commande/quantite')]
class ProduitCommandeQuantiteController extends AbstractController
{
    #[Route('/', name: 'app_produit_commande_quantite_index', methods: ['GET'])]
    public function index(EntityManagerInterface $entityManager): Response
    {
        $produitCommandeQuantites = $entityManager
            ->getRepository(ProduitCommandeQuantite::class)
            ->findAll();

        return $this->render('produit_commande_quantite/index.html.twig', [
            'produit_commande_quantites' => $produitCommandeQuantites,
        ]);
    }

    #[Route('/new', name: 'app_produit_commande_quantite_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $produitCommandeQuantite = new ProduitCommandeQuantite();
        $form = $this->createForm(ProduitCommandeQuantiteType::class, $produitCommandeQuantite);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($produitCommandeQuantite);
            $entityManager->flush();

            return $this->redirectToRoute('app_produit_commande_quantite_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('produit_commande_quantite/new.html.twig', [
            'produit_commande_quantite' => $produitCommandeQuantite,
            'form' => $form,
        ]);
    }

    #[Route('/{idProduitCommandeQuantite}', name: 'app_produit_commande_quantite_show', methods: ['GET'])]
    public function show(ProduitCommandeQuantite $produitCommandeQuantite): Response
    {
        return $this->render('produit_commande_quantite/show.html.twig', [
            'produit_commande_quantite' => $produitCommandeQuantite,
        ]);
    }

    #[Route('/{idProduitCommandeQuantite}/edit', name: 'app_produit_commande_quantite_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, ProduitCommandeQuantite $produitCommandeQuantite, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(ProduitCommandeQuantiteType::class, $produitCommandeQuantite);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_produit_commande_quantite_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('produit_commande_quantite/edit.html.twig', [
            'produit_commande_quantite' => $produitCommandeQuantite,
            'form' => $form,
        ]);
    }

    #[Route('/{idProduitCommandeQuantite}', name: 'app_produit_commande_quantite_delete', methods: ['POST'])]
    public function delete(Request $request, ProduitCommandeQuantite $produitCommandeQuantite, EntityManagerInterface $entityManager): Response
    {

        if ($this->isCsrfTokenValid('delete' . $produitCommandeQuantite->getIdProduitCommandeQuantite(), $request->request->get('_token'))) {

            $produitCommandeQuantite->DecrementationQuantite();
            $form = $this->createForm(ProduitCommandeQuantiteType::class, $produitCommandeQuantite);
            $form->handleRequest($request);

            $entityManager->flush();

        }

        return $this->redirectToRoute('app_produit_commande_quantite_index', [], Response::HTTP_SEE_OTHER);
    }
}
