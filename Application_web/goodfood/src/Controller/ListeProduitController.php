<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Restaurant;
use App\Entity\Produit;
use App\Entity\Menu;
use App\Entity\User;
use App\Entity\Commande;
use App\Entity\ProduitCommandeQuantite;
use App\Entity\MenuCommandeQuantite;

use App\Entity\Livreur;
use Symfony\Component\Security\Core\Security;

use App\Form\RestaurantType;
use App\Form\CommandeType;
use App\Form\ProduitCommandeQuantiteType;
use App\Form\MenuCommandeQuantiteType;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Id;
use SebastianBergmann\Environment\Console;
use Symfony\Component\HttpFoundation\Request;

class ListeProduitController extends AbstractController
{
    private $security;

    #[Route('/liste_produit', name: 'app_liste_produit')]
    public function index(EntityManagerInterface $entityManager): Response
    {        
        $Restaurants = $entityManager
        ->getRepository(Restaurant::class)
        ->findAll();
        
        $Produits = $entityManager
        ->getRepository(Produit::class)
        ->findAll();

        $Menus = $entityManager
        ->getRepository(Menu::class)
        ->findAll();


        return $this->render('liste_produit/index.html.twig', [
            'controller_name' => 'ListeProduitController',
            'Restaurants' => $Restaurants,
            'Produits' => $Produits,
            'Menus' => $Menus,
        ]);
    }


     /**
     * @Route("/{idproduit}/CommandeProduit", name="app_liste_produit_Commande_Produit", methods={"GET", "POST"})
     */
    public function CommandeProduit(Request $request,  Produit $Produit, EntityManagerInterface $entityManager): Response
    {
        $User = $this->security->getUser();

        $idutilisateur =  $User->getIdutilisateur()->getIdutilisateur();
        $idutilisateurString =   ''.$idutilisateur;

        $Livreur = $entityManager->getRepository(Livreur::class)->findOneBy(array('etats'=>'Disponible'));

        $Commande = $entityManager->getRepository(Commande::class)->findOneBy(array('idutilisateur' =>$idutilisateur ,'etats'=>'EnCours'),['idcommande' => 'DESC']);

        

        if($Commande==null)
        {
            $Commande = new Commande();
            $Commande->setAdresse('');
            $Commande->setEtats('EnCours');
            $Commande->setPrix(6);
            $Commande->setIdutilisateur($User->getIdutilisateur());
            $Commande->setIdlivreur($Livreur );
            $form = $this->createForm(CommandeType::class, $Commande);
            //$Commande->addIdProduitCommandeQuantite( $ProduitCommandeQuantite) ;

            $form->handleRequest($request);
            $entityManager->persist($Commande);

            $entityManager->flush();
            $Commande = $entityManager->getRepository(Commande::class)->findOneBy(array('idutilisateur' =>$idutilisateur ,'etats'=>'EnCours'),['idcommande' => 'DESC']);

        }

            $ProduitCommandeQuantite = $entityManager->getRepository(ProduitCommandeQuantite::class)->findOneBy(array('idproduit' =>$Produit->getIdproduit() ,'idcommande' =>$Commande->getIdcommande() ),['idProduitCommandeQuantite' => 'DESC']);

            if($ProduitCommandeQuantite == null)
            {
                $ProduitCommandeQuantite = new ProduitCommandeQuantite();
                $ProduitCommandeQuantite->setIdproduit($Produit);
                $ProduitCommandeQuantite->setIdcommande($Commande);
            }
      
    
            $ProduitCommandeQuantite->IncrementationQuantite();
    
            $form = $this->createForm(ProduitCommandeQuantiteType::class, $ProduitCommandeQuantite);
    
            $form->handleRequest($request);
            $entityManager->persist($ProduitCommandeQuantite);
            $entityManager->flush();
            
            $Commande->addIdProduitCommandeQuantite( $ProduitCommandeQuantite) ;
            $form = $this->createForm(CommandeType::class, $Commande);
            $form->handleRequest($request);
            $entityManager->flush();

        

     
        return $this->redirectToRoute('app_liste_produit', [], Response::HTTP_SEE_OTHER);
    }
     /**
     * @Route("/{idmenu}/CommandeMenu", name="app_liste_produit_Commande_Menu", methods={"GET", "POST"})
     */
    public function CommandeMenu(Request $request,  Menu $Menu  , EntityManagerInterface $entityManager): Response
    {
        $User = $this->security->getUser();

        $idutilisateur =  $User->getIdutilisateur()->getIdutilisateur();
        $idutilisateurString =   ''.$idutilisateur;

        $Livreur = $entityManager->getRepository(Livreur::class)->findOneBy(array('etats'=>'Disponible'));

        $Commande = $entityManager->getRepository(Commande::class)->findOneBy(array('idutilisateur' =>$idutilisateur ,'etats'=>'EnCours'),['idcommande' => 'DESC']);

        $MenuCommandeQuantite = $entityManager->getRepository(MenuCommandeQuantite::class)->findOneBy(array('idmenu' =>$Menu->getIdmenu() ,'idcommande' =>$Commande->getIdcommande() ),['idMenuCommandeQuantite' => 'DESC']);

        if($MenuCommandeQuantite == null)
        {
            $MenuCommandeQuantite = new MenuCommandeQuantite();
            $MenuCommandeQuantite->setIdmenu($Menu);
            $MenuCommandeQuantite->setIdcommande($Commande);
        }
  

        $MenuCommandeQuantite->IncrementationQuantite();

        $form = $this->createForm(MenuCommandeQuantiteType::class, $MenuCommandeQuantite);

        $form->handleRequest($request);
        $entityManager->persist($MenuCommandeQuantite);
        $entityManager->flush();

        if($Commande==null)
        {
            $Commande = new Commande();
            $Commande->setAdresse('');
            $Commande->setEtats('EnCours');
            $Commande->setPrix(6);
            $Commande->setIdutilisateur($User->getIdutilisateur());
            $Commande->setIdlivreur($Livreur );
            $form = $this->createForm(CommandeType::class, $Commande);
            $Commande->addIdMenuCommandeQuantite( $MenuCommandeQuantite) ;

            $form->handleRequest($request);
            $entityManager->persist($Commande);

            $entityManager->flush();
        }
        else
        {
            $Commande->addIdMenuCommandeQuantite( $MenuCommandeQuantite) ;
            $form = $this->createForm(CommandeType::class, $Commande);
            $form->handleRequest($request);
            $entityManager->flush();

        }

       return $this->redirectToRoute('app_liste_produit', [], Response::HTTP_SEE_OTHER);    
    }



    /*Ajout depuis le panier */

    
     /**
     * @Route("/{idproduit}/CommandeProduitPanier", name="app_liste_produit_Commande_Produit_Panier", methods={"GET", "POST"})
     */
    public function CommandeProduitPanier(Request $request,  Produit $Produit, EntityManagerInterface $entityManager): Response
    {
        $User = $this->security->getUser();

        $idutilisateur =  $User->getIdutilisateur()->getIdutilisateur();
        $idutilisateurString =   ''.$idutilisateur;

        $Livreur = $entityManager->getRepository(Livreur::class)->findOneBy(array('etats'=>'Disponible'));

        $Commande = $entityManager->getRepository(Commande::class)->findOneBy(array('idutilisateur' =>$idutilisateur ,'etats'=>'EnCours'),['idcommande' => 'DESC']);

        $ProduitCommandeQuantite = $entityManager->getRepository(ProduitCommandeQuantite::class)->findOneBy(array('idproduit' =>$Produit->getIdproduit() ,'idcommande' =>$Commande->getIdcommande() ),['idProduitCommandeQuantite' => 'DESC']);

        if($ProduitCommandeQuantite == null)
        {
            $ProduitCommandeQuantite = new ProduitCommandeQuantite();
            $ProduitCommandeQuantite->setIdproduit($Produit);
            $ProduitCommandeQuantite->setIdcommande($Commande);
        }
  

        $ProduitCommandeQuantite->IncrementationQuantite();

        $form = $this->createForm(ProduitCommandeQuantiteType::class, $ProduitCommandeQuantite);

        $form->handleRequest($request);
        $entityManager->persist($ProduitCommandeQuantite);
        $entityManager->flush();

        if($Commande==null)
        {
            $Commande = new Commande();
            $Commande->setAdresse('');
            $Commande->setEtats('EnCours');
            $Commande->setPrix(6);
            $Commande->setIdutilisateur($User->getIdutilisateur());
            $Commande->setIdlivreur($Livreur );
            $form = $this->createForm(CommandeType::class, $Commande);
            $Commande->addIdProduitCommandeQuantite( $ProduitCommandeQuantite) ;

            $form->handleRequest($request);
            $entityManager->persist($Commande);

            $entityManager->flush();
        }
        else
        {
            $Commande->addIdProduitCommandeQuantite( $ProduitCommandeQuantite) ;
            $form = $this->createForm(CommandeType::class, $Commande);
            $form->handleRequest($request);
            $entityManager->flush();

        }

        return $this->redirectToRoute('app_commande_EnCours', [], Response::HTTP_SEE_OTHER);
    }
     /**
     * @Route("/{idmenu}/CommandeMenuPanier", name="app_liste_produit_Commande_Menu_Panier", methods={"GET", "POST"})
     */
    public function CommandeMenuPanier(Request $request,  Menu $Menu  , EntityManagerInterface $entityManager): Response
    {
        $User = $this->security->getUser();

        $idutilisateur =  $User->getIdutilisateur()->getIdutilisateur();
        $idutilisateurString =   ''.$idutilisateur;

        $Livreur = $entityManager->getRepository(Livreur::class)->findOneBy(array('etats'=>'Disponible'));

        $Commande = $entityManager->getRepository(Commande::class)->findOneBy(array('idutilisateur' =>$idutilisateur ,'etats'=>'EnCours'),['idcommande' => 'DESC']);

        $MenuCommandeQuantite = $entityManager->getRepository(MenuCommandeQuantite::class)->findOneBy(array('idmenu' =>$Menu->getIdmenu() ,'idcommande' =>$Commande->getIdcommande() ),['idMenuCommandeQuantite' => 'DESC']);

        if($MenuCommandeQuantite == null)
        {
            $MenuCommandeQuantite = new MenuCommandeQuantite();
            $MenuCommandeQuantite->setIdmenu($Menu);
            $MenuCommandeQuantite->setIdcommande($Commande);
        }
  

        $MenuCommandeQuantite->IncrementationQuantite();

        $form = $this->createForm(MenuCommandeQuantiteType::class, $MenuCommandeQuantite);

        $form->handleRequest($request);
        $entityManager->persist($MenuCommandeQuantite);
        $entityManager->flush();

        if($Commande==null)
        {
            $Commande = new Commande();
            $Commande->setAdresse('');
            $Commande->setEtats('EnCours');
            $Commande->setPrix(6);
            $Commande->setIdutilisateur($User->getIdutilisateur());
            $Commande->setIdlivreur($Livreur );
            $form = $this->createForm(CommandeType::class, $Commande);
            $Commande->addIdMenuCommandeQuantite( $MenuCommandeQuantite) ;

            $form->handleRequest($request);
            $entityManager->persist($Commande);

            $entityManager->flush();
        }
        else
        {
            $Commande->addIdMenuCommandeQuantite( $MenuCommandeQuantite) ;
            $form = $this->createForm(CommandeType::class, $Commande);
            $form->handleRequest($request);
            $entityManager->flush();

        }

       return $this->redirectToRoute('app_commande_EnCours', [], Response::HTTP_SEE_OTHER);    
    }
    public function __construct(Security $security)
    {
       $this->security = $security;
    }




}
