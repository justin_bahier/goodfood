<?php

namespace App\Controller;

use App\Entity\Produit;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Id;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Restaurant;
use App\Form\RestaurantType;

/**
 * @Route("/liste_restaurant")
 */
class ListeRestaurantController extends AbstractController
{
    #[Route('/', name: 'app_liste_restaurant')]
    public function index(EntityManagerInterface $entityManager): Response
    {

        $Restaurants = $entityManager
        ->getRepository(Restaurant::class)
        ->findAll();


        return $this->render('liste_restaurant/index.html.twig', [
            'controller_name' => 'ListeRestaurantController',
            'Restaurants' => $Restaurants,


        ]);
    }

    /**
     * @Route("/new", name="app_liste_restaurant_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $Restaurant = new Restaurant();
        $form = $this->createForm(RestaurantType::class, $Restaurant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($Restaurant);
            $entityManager->flush();

            return $this->redirectToRoute('app_liste_restaurant', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('liste_restaurant/new.html.twig', [
            'Restaurant' => $Restaurant,
            'form' => $form,
        ]);
    }
/**
     * @Route("/{idrestaurant}/edit", name="app_liste_restaurant_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Restaurant $Restaurant, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(RestaurantType::class, $Restaurant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_liste_restaurant', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('liste_restaurant/edit.html.twig', [
            'Restaurant' => $Restaurant,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{idrestaurant}", name="app_liste_restaurant_delete", methods={"POST"})
     */
    public function delete(Request $request, Restaurant $Restaurant, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$Restaurant->getIdrestaurant(), $request->request->get('_token'))) {
            $entityManager->remove($Restaurant);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_liste_restaurant', [], Response::HTTP_SEE_OTHER);
    }
    /**
     * @Route("/{idrestaurant}", name="app_liste_restaurant_show", methods={"GET"})
     */
    public function show(Restaurant $Restaurant, EntityManagerInterface $entityManager): Response
    {
        $Produits = $entityManager->getRepository(Produit::class)->findBy(array('idrestaurant' => $Restaurant->getIdrestaurant()));


        return $this->render('liste_restaurant/show.html.twig', [
            'Restaurant' => $Restaurant,
            'Produits' => $Produits,
        ]);
    }

}
