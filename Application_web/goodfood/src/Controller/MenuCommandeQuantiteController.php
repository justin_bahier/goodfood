<?php

namespace App\Controller;

use App\Entity\MenuCommandeQuantite;
use App\Form\MenuCommandeQuantiteType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/menu/commande/quantite')]
class MenuCommandeQuantiteController extends AbstractController
{
    #[Route('/', name: 'app_menu_commande_quantite_index', methods: ['GET'])]
    public function index(EntityManagerInterface $entityManager): Response
    {
        $menuCommandeQuantites = $entityManager
            ->getRepository(MenuCommandeQuantite::class)
            ->findAll();

        return $this->render('menu_commande_quantite/index.html.twig', [
            'menu_commande_quantites' => $menuCommandeQuantites,
        ]);
    }

    #[Route('/new', name: 'app_menu_commande_quantite_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $menuCommandeQuantite = new MenuCommandeQuantite();
        $form = $this->createForm(MenuCommandeQuantiteType::class, $menuCommandeQuantite);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($menuCommandeQuantite);
            $entityManager->flush();

            return $this->redirectToRoute('app_menu_commande_quantite_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('menu_commande_quantite/new.html.twig', [
            'menu_commande_quantite' => $menuCommandeQuantite,
            'form' => $form,
        ]);
    }

    #[Route('/{idMenuCommandeQuantite}', name: 'app_menu_commande_quantite_show', methods: ['GET'])]
    public function show(MenuCommandeQuantite $menuCommandeQuantite): Response
    {
        return $this->render('menu_commande_quantite/show.html.twig', [
            'menu_commande_quantite' => $menuCommandeQuantite,
        ]);
    }

    #[Route('/{idMenuCommandeQuantite}/edit', name: 'app_menu_commande_quantite_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, MenuCommandeQuantite $menuCommandeQuantite, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(MenuCommandeQuantiteType::class, $menuCommandeQuantite);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('app_menu_commande_quantite_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('menu_commande_quantite/edit.html.twig', [
            'menu_commande_quantite' => $menuCommandeQuantite,
            'form' => $form,
        ]);
    }

    #[Route('/{idMenuCommandeQuantite}', name: 'app_menu_commande_quantite_delete', methods: ['POST'])]
    public function delete(Request $request, MenuCommandeQuantite $menuCommandeQuantite, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$menuCommandeQuantite->getIdMenuCommandeQuantite(), $request->request->get('_token'))) {
            $entityManager->remove($menuCommandeQuantite);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_menu_commande_quantite_index', [], Response::HTTP_SEE_OTHER);
    }
}
