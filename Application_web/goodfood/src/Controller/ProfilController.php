<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Utilisateur;

class ProfilController extends AbstractController
{
    #[Route('/profil', name: 'app_profil')]
    public function index(): Response
    {
        return $this->render('profil/index.html.twig', [
            'controller_name' => 'ProfilController',
        ]);
    }


        /**
     * @Route("/profil/{id}", name="app_Utilisateur_Page", methods={"GET"})
     */
    public function Page(Utilisateur $Utilisateur): Response
    {
        return $this->render('profil/ProfilShow.html.twig', [
            'Utilisateur' => $Utilisateur,
        ]);
    }

}
