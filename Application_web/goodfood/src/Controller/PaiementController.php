<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Commande;
use App\Form\PaiementType;
use Symfony\Component\Security\Core\Security;
use DateTime;
use DateInterval;
class PaiementController extends AbstractController
{

    private $security;
    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    #[Route('/paiement', name: 'app_paiement')]
    public function index(): Response
    {
        $minutes_to_add = 30;

        $time = new DateTime();
        $time->add(new DateInterval('PT' . $minutes_to_add . 'M'));

        $stamp = $time->format('H:i');

        return $this->render('paiement/index.html.twig', [
            'controller_name' => 'PaiementController',
            'HeureArrivé' => $stamp ,
        ]);
    }
    #[Route('/PayerCommande', name: 'app_paiement_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {

        $User = $this->security->getUser();

        $idutilisateur =  $User->getIdutilisateur()->getIdutilisateur();
        $idutilisateurString =   '' . $idutilisateur;
        $Commande = $entityManager->getRepository(Commande::class)->findOneBy(array('idutilisateur' => $idutilisateurString, 'etats' => 'EnCours'), ['idcommande' => 'DESC']);

        $prixtotal = 0;

        foreach ($Commande->getIdProduitCommandeQuantite() as $ProduitCommandeQuantite) {
            $prixtotal = $prixtotal + ($ProduitCommandeQuantite->getIdproduit()->getPrix() * $ProduitCommandeQuantite->getQuantite());
        }

        foreach ($Commande->getIdMenuCommandeQuantite() as $MenuCommandeQuantite) {
            $prixtotal = $prixtotal + ($MenuCommandeQuantite->getIdmenu()->getPrix() * $MenuCommandeQuantite->getQuantite());
        }


        $form = $this->createForm(PaiementType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {



            $CarteBancaire = $form->get('adresse')->getData('NumeroCarteBancaire')
                . $form->get('adresse')->getData('DateExpiration')
                . $form->get('adresse')->getData('Cvv')
                . $form->get('adresse')->getData('Nom');;

            $Commande->setAdresse($form->get('adresse')->getData());
            $Commande->setEtats('Livraison');

            $entityManager->persist($Commande);
            $entityManager->flush();


            return $this->redirectToRoute('app_paiement', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('paiement/Formulaire.html.twig', [
            'Commande' => $Commande,
            'form' => $form,
            'prixtotal' => $prixtotal,
        ]);
    }
}
