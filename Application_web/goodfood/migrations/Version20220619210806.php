<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220619210806 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE MenuCommandeQuantite (idmenu_id INT NOT NULL, idcommande_id INT NOT NULL, idMenuCommandeQuantite INT AUTO_INCREMENT NOT NULL, Quantite INT NOT NULL, INDEX IDX_BC75594C13D3BB7C (idmenu_id), INDEX IDX_BC75594C442B5EFB (idcommande_id), PRIMARY KEY(idMenuCommandeQuantite)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE MenuCommandeQuantite ADD CONSTRAINT FK_BC75594C13D3BB7C FOREIGN KEY (idmenu_id) REFERENCES menu (IdMenu)');
        $this->addSql('ALTER TABLE MenuCommandeQuantite ADD CONSTRAINT FK_BC75594C442B5EFB FOREIGN KEY (idcommande_id) REFERENCES commande (IdCommande)');
      }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE MenuCommandeQuantite');
     }
}
