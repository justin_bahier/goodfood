<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220619195231 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE ProduitCommandeQuantite (idproduit_id INT NOT NULL, idcommande_id INT NOT NULL, idProduitCommandeQuantite INT AUTO_INCREMENT NOT NULL, Quantite INT NOT NULL, INDEX IDX_B878EBE9C29D63C1 (idproduit_id), INDEX IDX_B878EBE9442B5EFB (idcommande_id), PRIMARY KEY(idProduitCommandeQuantite)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ProduitCommandeQuantite ADD CONSTRAINT FK_B878EBE9C29D63C1 FOREIGN KEY (idproduit_id) REFERENCES produit (IdProduit)');
        $this->addSql('ALTER TABLE ProduitCommandeQuantite ADD CONSTRAINT FK_B878EBE9442B5EFB FOREIGN KEY (idcommande_id) REFERENCES commande (IdCommande)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE ProduitCommandeQuantite');
    }
}
