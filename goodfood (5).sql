-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : lun. 20 juin 2022 à 22:44
-- Version du serveur : 10.4.22-MariaDB
-- Version de PHP : 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `goodfood`
--

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

CREATE TABLE `commande` (
  `IdCommande` int(11) NOT NULL,
  `Adresse` varchar(30) NOT NULL,
  `Prix` int(11) NOT NULL,
  `Etats` varchar(50) NOT NULL,
  `IdUtilisateur` int(11) DEFAULT NULL,
  `IdLivreur` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `commande`
--

INSERT INTO `commande` (`IdCommande`, `Adresse`, `Prix`, `Etats`, `IdUtilisateur`, `IdLivreur`) VALUES
(1, 'chez justin', 10, 'fini', 2, 1),
(2, 'taff justin', 15, 'Fini', 2, 2),
(3, 'chez josselin', 9, 'fini', 1, 2),
(4, 'nantes france', 6, 'Fini', 2, NULL),
(5, 'eeee', 3, 'fini', 2, NULL),
(6, 'Le Clos Beaucet', 6, 'Livraison', 2, 1),
(7, '', 6, 'Annulé', 2, 1),
(8, '', 6, 'Annulé', 2, 1),
(9, '', 6, 'Annulé', 2, 1),
(10, '', 6, 'Annulé', 2, 1),
(11, '', 6, 'Annulé', 2, 1),
(12, '', 6, 'Annulé', 2, 1);

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20220610204932', '2022-06-16 11:45:38', 65),
('DoctrineMigrations\\Version20220610205751', '2022-06-16 11:45:38', 1),
('DoctrineMigrations\\Version20220616114328', '2022-06-16 11:45:38', 20),
('DoctrineMigrations\\Version20220616123044', '2022-06-16 12:40:50', 75),
('DoctrineMigrations\\Version20220616124459', '2022-06-16 12:45:46', 221),
('DoctrineMigrations\\Version20220617184328', '2022-06-17 18:45:08', 128),
('DoctrineMigrations\\Version20220618123025', '2022-06-18 12:31:11', 116),
('DoctrineMigrations\\Version20220618140640', '2022-06-18 14:07:27', 119),
('DoctrineMigrations\\Version20220618141035', '2022-06-18 14:11:23', 137),
('DoctrineMigrations\\Version20220618151117', '2022-06-18 23:03:29', 112),
('DoctrineMigrations\\Version20220618230302', '2022-06-19 19:55:14', 68),
('DoctrineMigrations\\Version20220619195231', '2022-06-19 19:55:14', 248),
('DoctrineMigrations\\Version20220619210806', '2022-06-19 21:08:34', 308);

-- --------------------------------------------------------

--
-- Structure de la table `livreur`
--

CREATE TABLE `livreur` (
  `IdLivreur` int(11) NOT NULL,
  `Email` varchar(30) NOT NULL,
  `MotsPasse` varchar(30) NOT NULL,
  `Pseudo` varchar(30) NOT NULL,
  `Telephone` varchar(30) NOT NULL,
  `Photo` varchar(100) NOT NULL,
  `Nom` varchar(30) NOT NULL,
  `Prenom` varchar(30) NOT NULL,
  `RIB` varchar(30) NOT NULL,
  `Etats` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `livreur`
--

INSERT INTO `livreur` (`IdLivreur`, `Email`, `MotsPasse`, `Pseudo`, `Telephone`, `Photo`, `Nom`, `Prenom`, `RIB`, `Etats`) VALUES
(1, 'SoeuraJosselin@gmail.com', 'root', 'SoeuraJosselin', '0123456', '', 'aJosselin', 'Soeur', '123456', 'Disponible'),
(2, 'gerad@gmail.com', 'root', 'gerad', '0123456', '', 'menvusa', 'gerad', '123456', 'Disponible');

-- --------------------------------------------------------

--
-- Structure de la table `menu`
--

CREATE TABLE `menu` (
  `IdMenu` int(11) NOT NULL,
  `Nom` varchar(50) NOT NULL,
  `Description` varchar(50) NOT NULL,
  `Prix` int(11) NOT NULL,
  `Photo` varchar(50) NOT NULL,
  `IdRestaurant` int(11) DEFAULT NULL,
  `Quantite` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `menu`
--

INSERT INTO `menu` (`IdMenu`, `Nom`, `Description`, `Prix`, `Photo`, `IdRestaurant`, `Quantite`) VALUES
(1, 'TAILLE M', 'tacos avec Une dose de viande au choix et coca', 5, '', 1, 0),
(2, 'TAILLE M', 'tacos avec Une dose de viande au choix et fanta', 5, '', 1, 0),
(3, 'Menu burger frite  boisson', 'Menu burger frite  boisson', 8, '', 2, 0),
(4, 'MENU PITAYA', 'PLAT + BOISSON CLASSIQUE', 10, '', 3, 0);

-- --------------------------------------------------------

--
-- Structure de la table `menucommande`
--

CREATE TABLE `menucommande` (
  `IdCommande` int(11) NOT NULL,
  `IdMenu` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `menucommande`
--

INSERT INTO `menucommande` (`IdCommande`, `IdMenu`) VALUES
(1, 4),
(3, 3),
(4, 4),
(5, 1),
(5, 2);

-- --------------------------------------------------------

--
-- Structure de la table `menucommandequantite`
--

CREATE TABLE `menucommandequantite` (
  `idmenu_id` int(11) NOT NULL,
  `idcommande_id` int(11) NOT NULL,
  `idMenuCommandeQuantite` int(11) NOT NULL,
  `Quantite` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `menucommandequantite`
--

INSERT INTO `menucommandequantite` (`idmenu_id`, `idcommande_id`, `idMenuCommandeQuantite`, `Quantite`) VALUES
(1, 6, 1, 3),
(2, 6, 2, 1);

-- --------------------------------------------------------

--
-- Structure de la table `menuproduit`
--

CREATE TABLE `menuproduit` (
  `IdMenu` int(11) NOT NULL,
  `IdProduit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `menuproduit`
--

INSERT INTO `menuproduit` (`IdMenu`, `IdProduit`) VALUES
(1, 1),
(1, 4),
(2, 1),
(2, 7),
(3, 9),
(3, 13),
(4, 8),
(4, 14);

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

CREATE TABLE `produit` (
  `IdProduit` int(11) NOT NULL,
  `Nom` varchar(30) NOT NULL,
  `Description` varchar(250) NOT NULL,
  `DateCreation` date NOT NULL,
  `Prix` int(11) NOT NULL,
  `Photo` varchar(50) NOT NULL,
  `IdRestaurant` int(11) NOT NULL,
  `Quantite` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `produit`
--

INSERT INTO `produit` (`IdProduit`, `Nom`, `Description`, `DateCreation`, `Prix`, `Photo`, `IdRestaurant`, `Quantite`) VALUES
(1, 'tacos M', 'Tacos avec Une dose de viande au choix\r\n\r\n', '2022-06-12', 5, '', 1, 0),
(2, 'tacos L', 'Tacos avec Double dose de viande au choix\r\n\r\n', '2022-06-12', 7, 'TacosMexicanos.jpeg', 1, 0),
(3, 'tacos XL', 'Tacos avec Double tortilla + triple dose de viande au choix\r\n\r\n', '2022-06-12', 9, '', 1, 0),
(4, 'Coca Cola', 'Coca-Cola, parfois abrégé Coca ou Cola dans les pays francophones ou Coke en Amérique du Nord et dans certains pays européens et africains, est une marque nord américaine de soda de type cola ', '2022-06-12', 1, '', 1, 0),
(5, 'Coca Cola', 'Coca-Cola, parfois abrégé Coca ou Cola dans les pays francophones ou Coke en Amérique du Nord et dans certains pays européens et africains, est une marque nord américaine de soda de type cola ', '2022-06-12', 1, '', 2, 0),
(6, 'Coca Cola', 'Coca-Cola, parfois abrégé Coca ou Cola dans les pays francophones ou Coke en Amérique du Nord et dans certains pays européens et africains, est une marque nord américaine de soda de type cola ', '2022-06-12', 1, '', 3, 0),
(7, 'Fanta', 'Fanta est une marque de boissons gazeuses aromatisées aux fruits créée par Coca-Cola Deutschland sous la direction de l\'homme d\'affaires allemand Max Keith. Il existe plus de 200 saveurs dans le monde.', '2022-06-12', 1, '', 1, 0),
(8, 'Fanta', 'Fanta est une marque de boissons gazeuses aromatisées aux fruits créée par Coca-Cola Deutschland sous la direction de l\'homme d\'affaires allemand Max Keith. Il existe plus de 200 saveurs dans le monde.', '2022-06-12', 1, '', 2, 0),
(9, 'Fanta', 'Fanta est une marque de boissons gazeuses aromatisées aux fruits créée par Coca-Cola Deutschland sous la direction de l\'homme d\'affaires allemand Max Keith. Il existe plus de 200 saveurs dans le monde.', '2022-06-12', 1, '', 3, 0),
(10, 'Oasis Tropical', 'Oasis Tropical est une boisson plate à base de 12% de concentré de fruits composé d’oranges, de pommes, de fruits de la Passion et de la mangue ainsi que de 80% d’eau de source dépourvue de conservateur et de colorant artificiel.\r\n\r\n', '2022-06-12', 1, '', 1, 0),
(11, 'Oasis Tropical', 'Oasis Tropical est une boisson plate à base de 12% de concentré de fruits composé d’oranges, de pommes, de fruits de la Passion et de la mangue ainsi que de 80% d’eau de source dépourvue de conservateur et de colorant artificiel.\r\n\r\n', '2022-06-12', 1, '', 2, 0),
(12, 'Oasis Tropical', 'Oasis Tropical est une boisson plate à base de 12% de concentré de fruits composé d’oranges, de pommes, de fruits de la Passion et de la mangue ainsi que de 80% d’eau de source dépourvue de conservateur et de colorant artificiel.\r\n\r\n', '2022-06-12', 1, '', 3, 0),
(13, 'hanburger', 'tomate , salade pickle , sauce roadside , oignon rouges', '2022-06-12', 6, '', 3, 0),
(14, 'SIE YAI', 'Émincé de boeuf, nouilles de blé Udon, pousses de soja et carottes sautés au wok, ciboulette thaï, oignons frits.', '2022-06-12', 9, '', 2, 0),
(15, 'kebab', 'kebab\r\n\r\n', '2022-06-12', 5, '', 1, 0),
(16, 'swandich', 'swandich\r\n\r\n', '2022-06-12', 7, '', 1, 0),
(17, 'ICE TEA', 'ICE TEA ', '2022-06-12', 7, '', 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `produitcommande`
--

CREATE TABLE `produitcommande` (
  `IdProduit` int(11) NOT NULL,
  `IdCommande` int(11) NOT NULL,
  `nombre` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `produitcommande`
--

INSERT INTO `produitcommande` (`IdProduit`, `IdCommande`, `nombre`) VALUES
(1, 2, 3),
(1, 4, 0),
(1, 6, 0),
(2, 4, 0),
(2, 6, 0),
(3, 4, 0),
(17, 4, 0);

-- --------------------------------------------------------

--
-- Structure de la table `produitcommandequantite`
--

CREATE TABLE `produitcommandequantite` (
  `idproduit_id` int(11) NOT NULL,
  `idcommande_id` int(11) NOT NULL,
  `idProduitCommandeQuantite` int(11) NOT NULL,
  `Quantite` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `produitcommandequantite`
--

INSERT INTO `produitcommandequantite` (`idproduit_id`, `idcommande_id`, `idProduitCommandeQuantite`, `Quantite`) VALUES
(1, 6, 1, 2),
(2, 6, 3, 2),
(4, 7, 4, 2),
(8, 7, 5, 1),
(2, 8, 6, 1),
(2, 9, 7, 1),
(2, 10, 8, 1),
(1, 11, 9, 1),
(1, 12, 10, 1);

-- --------------------------------------------------------

--
-- Structure de la table `restaurant`
--

CREATE TABLE `restaurant` (
  `IdRestaurant` int(11) NOT NULL,
  `Nom` varchar(30) NOT NULL,
  `Description` varchar(250) NOT NULL,
  `Siret` varchar(30) NOT NULL,
  `SiteInternet` varchar(30) NOT NULL,
  `Adresse` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `restaurant`
--

INSERT INTO `restaurant` (`IdRestaurant`, `Nom`, `Description`, `Siret`, `SiteInternet`, `Adresse`) VALUES
(1, 'otacos', 'O\'Tacos est une chaîne de restauration rapide française créée en 2007 à Grenoble et spécialisée dans les tacos français', '809849615', 'https://o-tacos.com/fr/', '2 Cr Olivier de Clisson, 44000'),
(2, 'Pitaya Thaï Street Food', 'Chez Pitaya vous retrouverez l’esprit des rues de Bangkok, les odeurs qui émanent des petites échoppes de street food traditionnelles, les couleurs des produits frais, les gestes précis des chefs,  et l’effervescence de la Thaïlande. Tout se joue sou', '85253707500024', 'pitaya-thaistreetfood.com', '4 Rue de Gorges, 44000 Nantes'),
(3, 'roadside', 'ROADSIDE est un nouveau Fast Food convivial et authentique servant des Burgers et des Hot Dogs préparés à la commande, à base de produits frais, ainsi que des Milkshakes, une offre de Coffee Shop et plus encore !\r\n', '903943298', 'roadside.fr', '1 All. Flesselles, 44000 Nante');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:json)',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IdUtilisateur` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `email`, `roles`, `password`, `IdUtilisateur`) VALUES
(1, 'justin@test', '[]', '$2y$13$uoWta7VVE9FgDT5aIYcUiOWDqj3ot9wl9l/lEVJsXKpIKlB.wb4W6', 2);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `IdUtilisateur` int(11) NOT NULL,
  `Email` varchar(30) NOT NULL,
  `MotsPasse` varchar(30) NOT NULL,
  `Pseudo` varchar(30) NOT NULL,
  `Telephone` varchar(30) NOT NULL,
  `Photo` varchar(100) NOT NULL,
  `Nom` varchar(30) NOT NULL,
  `Prenom` varchar(30) NOT NULL,
  `Adresse` varchar(30) NOT NULL,
  `CarteBancaire` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`IdUtilisateur`, `Email`, `MotsPasse`, `Pseudo`, `Telephone`, `Photo`, `Nom`, `Prenom`, `Adresse`, `CarteBancaire`) VALUES
(1, 'Josselin@gmail.com', 'root', 'Josselin', '0123456', '', 'Bourdelle', 'Josselin', 'france le mans', '123456'),
(2, 'justin@gmail.com', 'root', 'justin', '123456879', '', 'justin', 'bahier', 'nantes france', '123456'),
(3, 'clement@gmail.Com', 'root', 'clement', '0123456', '', 'Fourrier', 'clement', 'france le mans', '123456');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `commande`
--
ALTER TABLE `commande`
  ADD PRIMARY KEY (`IdCommande`),
  ADD KEY `Commande_Utilisateur_FK` (`IdUtilisateur`),
  ADD KEY `Commande_Livreur0_FK` (`IdLivreur`);

--
-- Index pour la table `doctrine_migration_versions`
--
ALTER TABLE `doctrine_migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Index pour la table `livreur`
--
ALTER TABLE `livreur`
  ADD PRIMARY KEY (`IdLivreur`);

--
-- Index pour la table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`IdMenu`),
  ADD KEY `IdRestaurant` (`IdRestaurant`);

--
-- Index pour la table `menucommande`
--
ALTER TABLE `menucommande`
  ADD PRIMARY KEY (`IdCommande`,`IdMenu`),
  ADD KEY `IDX_92795ABE954613B` (`IdMenu`);

--
-- Index pour la table `menucommandequantite`
--
ALTER TABLE `menucommandequantite`
  ADD PRIMARY KEY (`idMenuCommandeQuantite`),
  ADD KEY `IDX_BC75594C13D3BB7C` (`idmenu_id`),
  ADD KEY `IDX_BC75594C442B5EFB` (`idcommande_id`);

--
-- Index pour la table `menuproduit`
--
ALTER TABLE `menuproduit`
  ADD PRIMARY KEY (`IdMenu`,`IdProduit`),
  ADD KEY `MenuProduit_Produit0_FK` (`IdProduit`);

--
-- Index pour la table `produit`
--
ALTER TABLE `produit`
  ADD PRIMARY KEY (`IdProduit`),
  ADD KEY `IdRestaurant` (`IdRestaurant`);

--
-- Index pour la table `produitcommande`
--
ALTER TABLE `produitcommande`
  ADD PRIMARY KEY (`IdProduit`,`IdCommande`),
  ADD KEY `ProduitCommande_Commande0_FK` (`IdCommande`);

--
-- Index pour la table `produitcommandequantite`
--
ALTER TABLE `produitcommandequantite`
  ADD PRIMARY KEY (`idProduitCommandeQuantite`),
  ADD KEY `IDX_B878EBE9C29D63C1` (`idproduit_id`),
  ADD KEY `IDX_B878EBE9442B5EFB` (`idcommande_id`);

--
-- Index pour la table `restaurant`
--
ALTER TABLE `restaurant`
  ADD PRIMARY KEY (`IdRestaurant`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`),
  ADD KEY `IDX_8D93D649EE3FD73E` (`IdUtilisateur`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`IdUtilisateur`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `commande`
--
ALTER TABLE `commande`
  MODIFY `IdCommande` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT pour la table `livreur`
--
ALTER TABLE `livreur`
  MODIFY `IdLivreur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `menu`
--
ALTER TABLE `menu`
  MODIFY `IdMenu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `menucommandequantite`
--
ALTER TABLE `menucommandequantite`
  MODIFY `idMenuCommandeQuantite` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `produit`
--
ALTER TABLE `produit`
  MODIFY `IdProduit` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT pour la table `produitcommandequantite`
--
ALTER TABLE `produitcommandequantite`
  MODIFY `idProduitCommandeQuantite` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `restaurant`
--
ALTER TABLE `restaurant`
  MODIFY `IdRestaurant` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `IdUtilisateur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `commande`
--
ALTER TABLE `commande`
  ADD CONSTRAINT `Commande_Livreur0_FK` FOREIGN KEY (`IdLivreur`) REFERENCES `livreur` (`IdLivreur`),
  ADD CONSTRAINT `Commande_Utilisateur_FK` FOREIGN KEY (`IdUtilisateur`) REFERENCES `utilisateur` (`IdUtilisateur`);

--
-- Contraintes pour la table `menu`
--
ALTER TABLE `menu`
  ADD CONSTRAINT `FK_7D053A937150D72D` FOREIGN KEY (`IdRestaurant`) REFERENCES `restaurant` (`IdRestaurant`);

--
-- Contraintes pour la table `menucommande`
--
ALTER TABLE `menucommande`
  ADD CONSTRAINT `FK_92795ABE954613B` FOREIGN KEY (`IdMenu`) REFERENCES `menu` (`IdMenu`),
  ADD CONSTRAINT `MenuCommande_Commande_FK` FOREIGN KEY (`IdCommande`) REFERENCES `commande` (`IdCommande`);

--
-- Contraintes pour la table `menucommandequantite`
--
ALTER TABLE `menucommandequantite`
  ADD CONSTRAINT `FK_BC75594C13D3BB7C` FOREIGN KEY (`idmenu_id`) REFERENCES `menu` (`IdMenu`),
  ADD CONSTRAINT `FK_BC75594C442B5EFB` FOREIGN KEY (`idcommande_id`) REFERENCES `commande` (`IdCommande`);

--
-- Contraintes pour la table `menuproduit`
--
ALTER TABLE `menuproduit`
  ADD CONSTRAINT `MenuProduit_Menu_FK` FOREIGN KEY (`IdMenu`) REFERENCES `menu` (`IdMenu`),
  ADD CONSTRAINT `MenuProduit_Produit0_FK` FOREIGN KEY (`IdProduit`) REFERENCES `produit` (`IdProduit`);

--
-- Contraintes pour la table `produit`
--
ALTER TABLE `produit`
  ADD CONSTRAINT `Produit_Restaurant_FK` FOREIGN KEY (`IdRestaurant`) REFERENCES `restaurant` (`IdRestaurant`);

--
-- Contraintes pour la table `produitcommande`
--
ALTER TABLE `produitcommande`
  ADD CONSTRAINT `ProduitCommande_Commande0_FK` FOREIGN KEY (`IdCommande`) REFERENCES `commande` (`IdCommande`),
  ADD CONSTRAINT `ProduitCommande_Produit_FK` FOREIGN KEY (`IdProduit`) REFERENCES `produit` (`IdProduit`);

--
-- Contraintes pour la table `produitcommandequantite`
--
ALTER TABLE `produitcommandequantite`
  ADD CONSTRAINT `FK_B878EBE9442B5EFB` FOREIGN KEY (`idcommande_id`) REFERENCES `commande` (`IdCommande`),
  ADD CONSTRAINT `FK_B878EBE9C29D63C1` FOREIGN KEY (`idproduit_id`) REFERENCES `produit` (`IdProduit`);

--
-- Contraintes pour la table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `FK_8D93D649EE3FD73E` FOREIGN KEY (`IdUtilisateur`) REFERENCES `utilisateur` (`IdUtilisateur`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
